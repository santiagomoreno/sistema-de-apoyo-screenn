-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: todoinversiones
-- ------------------------------------------------------
-- Server version	5.6.15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activo`
--

DROP TABLE IF EXISTS `activo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activo` (
  `idactivo` int(11) NOT NULL,
  `idproducto` varchar(20) NOT NULL,
  `idCliente` int(11) DEFAULT NULL,
  `idpublicacionventa` int(11) DEFAULT NULL,
  PRIMARY KEY (`idactivo`),
  KEY `producto_cliente_PK_idx` (`idCliente`),
  KEY `producto_activo_PK_idx` (`idproducto`),
  KEY `publicacionV_activo_PK_idx` (`idpublicacionventa`),
  CONSTRAINT `cliente_activo_Pk` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `producto_activo_PK` FOREIGN KEY (`idproducto`) REFERENCES `producto` (`idproducto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `publicacionV_activo_PK` FOREIGN KEY (`idpublicacionventa`) REFERENCES `publicacionventa` (`idpublicacionventa`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activo`
--

LOCK TABLES `activo` WRITE;
/*!40000 ALTER TABLE `activo` DISABLE KEYS */;
INSERT INTO `activo` VALUES (1,'ALUA',NULL,1),(2,'ALUA',NULL,1),(3,'ALUA',NULL,2),(4,'ALUA',NULL,2),(5,'ALUA',NULL,2),(6,'ALUA',NULL,3),(7,'ALUA',NULL,4),(8,'ALUA',NULL,4),(9,'ALUA',NULL,4),(10,'ALUA',NULL,4),(11,'ALUA',NULL,5),(12,'ALUA',NULL,5),(13,'ALUA',NULL,6),(14,'ALUA',NULL,6),(15,'ALUA',NULL,6),(16,'ALUA',NULL,6),(17,'ALUA',NULL,6),(18,'BMA',NULL,7),(19,'BMA',NULL,7),(20,'BMA',NULL,8),(21,'BMA',NULL,9),(22,'BMA',NULL,10),(23,'BMA',NULL,10),(24,'BMA',NULL,10),(25,'BMA',NULL,11),(26,'BMA',NULL,11),(27,'BMA',NULL,12),(28,'BMA',NULL,12),(29,'BMA',NULL,12),(30,'BMA',NULL,12),(31,'BYMA',NULL,13),(32,'BYMA',NULL,13),(33,'BYMA',NULL,13),(34,'BYMA',NULL,13),(35,'BYMA',NULL,14),(36,'BYMA',NULL,15),(37,'BYMA',NULL,15),(38,'BYMA',NULL,15),(39,'BYMA',NULL,16),(40,'BYMA',NULL,16),(41,'BYMA',NULL,17),(42,'BYMA',NULL,17),(43,'BYMA',NULL,17),(44,'BYMA',NULL,18),(45,'CEPU',NULL,19),(46,'CEPU',NULL,19),(47,'CEPU',NULL,19),(48,'CEPU',NULL,19),(49,'CEPU',NULL,20),(50,'CEPU',NULL,20),(51,'CEPU',NULL,21),(52,'CEPU',NULL,21),(53,'CEPU',NULL,22),(54,'CEPU',NULL,23),(55,'CEPU',NULL,24),(56,'CEPU',NULL,24),(57,'CEPU',NULL,24),(58,'CEPU',NULL,24),(59,'BTC',NULL,25),(60,'BTC',NULL,25),(61,'BTC',NULL,25),(62,'BTC',NULL,25),(63,'BTC',NULL,26),(64,'BTC',NULL,26),(65,'BTC',NULL,26),(66,'BTC',NULL,26),(67,'BTC',NULL,26),(68,'BTC',NULL,27),(69,'BTC',NULL,27),(70,'BTC',NULL,28),(71,'BTC',NULL,29),(72,'BTC',NULL,29),(73,'BTC',NULL,30),(74,'BTC',NULL,30),(75,'BTC',NULL,30),(76,'ETH',NULL,31),(77,'ETH',NULL,32),(78,'ETH',NULL,33),(79,'ETH',NULL,33),(80,'ETH',NULL,33),(81,'ETH',NULL,34),(82,'ETH',NULL,34),(83,'ETH',NULL,35),(84,'ETH',NULL,35),(85,'ETH',NULL,35),(86,'ETH',NULL,36),(87,'ETH',NULL,36),(88,'ETH',NULL,36),(89,'ETH',NULL,36),(90,'XRP',NULL,37),(91,'XRP',NULL,37),(92,'XRP',NULL,38),(93,'XRP',NULL,39),(94,'XRP',NULL,39),(95,'XRP',NULL,39),(96,'XRP',NULL,39),(97,'XRP',NULL,39),(98,'XRP',NULL,40),(99,'XRP',NULL,40),(100,'XRP',NULL,40),(101,'XRP',NULL,41),(102,'XRP',NULL,41),(103,'XRP',NULL,42);
/*!40000 ALTER TABLE `activo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banco`
--

DROP TABLE IF EXISTS `banco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banco` (
  `idBanco` int(11) NOT NULL AUTO_INCREMENT,
  `nombreBanco` varchar(45) NOT NULL,
  PRIMARY KEY (`idBanco`),
  UNIQUE KEY `nombreBanco_UNIQUE` (`nombreBanco`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banco`
--

LOCK TABLES `banco` WRITE;
/*!40000 ALTER TABLE `banco` DISABLE KEYS */;
INSERT INTO `banco` VALUES (1,'BANCO DE GALICIA Y BUENOS AIRES S.A.U.'),(10,'BANCO DE LA CIUDAD DE BUENOS AIRES'),(2,'BANCO DE LA NACION ARGENTINA'),(3,'BANCO DE LA PROVINCIA DE BUENOS AIRES'),(8,'BANCO DE LA PROVINCIA DE CORDOBA S.A.'),(9,'BANCO SUPERVIELLE S.A.'),(6,'BBVA BANCO FRANCES S.A.'),(5,'CITIBANK N.A.'),(4,'INDUSTRIAL AND COMMERCIAL BANK OF CHINA'),(7,'MUFG BANK, LTD');
/*!40000 ALTER TABLE `banco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `idcategoria` int(11) NOT NULL AUTO_INCREMENT,
  `nombrecategoria` varchar(45) NOT NULL,
  PRIMARY KEY (`idcategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (1,'Acciones'),(2,'Bonos'),(3,'Criptomonedas');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `idCliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `domicilio` varchar(50) NOT NULL,
  `dni` varchar(50) NOT NULL,
  `fechaNacimiento` date NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `fechaIngreso` date NOT NULL,
  `cuentaComitente` varchar(100) NOT NULL,
  `perfilDeInversor` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCliente`),
  UNIQUE KEY `cuentaComitente_UNIQUE` (`cuentaComitente`),
  UNIQUE KEY `dni_UNIQUE` (`dni`),
  KEY `clForaneaPerfilDeInversor_idx` (`perfilDeInversor`),
  CONSTRAINT `clForaneaCuentaComitente` FOREIGN KEY (`cuentaComitente`) REFERENCES `cuentacomitente` (`nroCuenta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `clForaneaPerfilDeInversor` FOREIGN KEY (`perfilDeInversor`) REFERENCES `perfildeinversor` (`idperfilDeInversor`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (1,'dfdfd','fdfdfd','dfdfdf','dfdd','2019-05-01','ssssds@.fhdff','4454fss','2019-05-21','45454545454424',NULL);
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuentacomitente`
--

DROP TABLE IF EXISTS `cuentacomitente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuentacomitente` (
  `nroCuenta` varchar(100) NOT NULL,
  `saldo` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`nroCuenta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuentacomitente`
--

LOCK TABLES `cuentacomitente` WRITE;
/*!40000 ALTER TABLE `cuentacomitente` DISABLE KEYS */;
INSERT INTO `cuentacomitente` VALUES ('454545252525',0),('45454545454424',0);
/*!40000 ALTER TABLE `cuentacomitente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `encuestasatisfaccion`
--

DROP TABLE IF EXISTS `encuestasatisfaccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `encuestasatisfaccion` (
  `idencuestasatisfaccion` int(11) NOT NULL AUTO_INCREMENT,
  `cliente` int(11) NOT NULL,
  `estadodesatisfaccion` int(11) NOT NULL,
  PRIMARY KEY (`idencuestasatisfaccion`),
  KEY `clvforaneaclienteencuesta_idx` (`cliente`),
  KEY `clvforaneaestadosatisfaccion_idx` (`estadodesatisfaccion`),
  CONSTRAINT `clvforaneaclienteencuesta` FOREIGN KEY (`cliente`) REFERENCES `cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `clvforaneaestadosatisfaccion` FOREIGN KEY (`estadodesatisfaccion`) REFERENCES `estadosatisfaccion` (`idestadosatisfaccion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `encuestasatisfaccion`
--

LOCK TABLES `encuestasatisfaccion` WRITE;
/*!40000 ALTER TABLE `encuestasatisfaccion` DISABLE KEYS */;
INSERT INTO `encuestasatisfaccion` VALUES (1,1,3),(2,1,1);
/*!40000 ALTER TABLE `encuestasatisfaccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estadosatisfaccion`
--

DROP TABLE IF EXISTS `estadosatisfaccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadosatisfaccion` (
  `idestadosatisfaccion` int(11) NOT NULL,
  `estadosatisfaccion` varchar(45) NOT NULL,
  PRIMARY KEY (`idestadosatisfaccion`),
  UNIQUE KEY `estadosatisfaccion_UNIQUE` (`estadosatisfaccion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadosatisfaccion`
--

LOCK TABLES `estadosatisfaccion` WRITE;
/*!40000 ALTER TABLE `estadosatisfaccion` DISABLE KEYS */;
INSERT INTO `estadosatisfaccion` VALUES (2,'Conforme'),(1,'Desconforme'),(3,'MuySatisfecho');
/*!40000 ALTER TABLE `estadosatisfaccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fondo`
--

DROP TABLE IF EXISTS `fondo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fondo` (
  `idFondo` int(11) NOT NULL,
  `Precio` float DEFAULT NULL,
  PRIMARY KEY (`idFondo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fondo`
--

LOCK TABLES `fondo` WRITE;
/*!40000 ALTER TABLE `fondo` DISABLE KEYS */;
/*!40000 ALTER TABLE `fondo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfildeinversor`
--

DROP TABLE IF EXISTS `perfildeinversor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfildeinversor` (
  `idperfilDeInversor` int(11) NOT NULL AUTO_INCREMENT,
  `perfilDeInversor` varchar(45) NOT NULL,
  PRIMARY KEY (`idperfilDeInversor`),
  UNIQUE KEY `perfilDeInversor_UNIQUE` (`perfilDeInversor`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfildeinversor`
--

LOCK TABLES `perfildeinversor` WRITE;
/*!40000 ALTER TABLE `perfildeinversor` DISABLE KEYS */;
INSERT INTO `perfildeinversor` VALUES (3,'Arriesgado'),(1,'Conservador'),(2,'Moderado');
/*!40000 ALTER TABLE `perfildeinversor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `idproducto` varchar(20) NOT NULL,
  `nombreproducto` varchar(45) NOT NULL,
  `idcategoria` int(11) NOT NULL,
  `idperfilDeInversor` int(11) DEFAULT NULL,
  PRIMARY KEY (`idproducto`),
  KEY `categoria_producto_PK_idx` (`idcategoria`),
  KEY `perfil_producto_PK_idx` (`idperfilDeInversor`),
  CONSTRAINT `perfil_producto_PK` FOREIGN KEY (`idperfilDeInversor`) REFERENCES `perfildeinversor` (`idperfilDeInversor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `categoria_producto_PK` FOREIGN KEY (`idcategoria`) REFERENCES `categoria` (`idcategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES ('ALUA','Aluar',1,NULL),('BMA','Banco Macro',1,NULL),('BTC','Bitcoin',3,NULL),('BYMA','Bolsas Y Mercados Argentinos S.A.',1,NULL),('CEPU','Central Puerto S.A.',1,NULL),('ETH','Ethereum',3,NULL),('XRP','Ripple',3,NULL);
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publicacioncompra`
--

DROP TABLE IF EXISTS `publicacioncompra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publicacioncompra` (
  `idpublicacioncompra` int(11) NOT NULL,
  `precio` float NOT NULL,
  `idproducto` varchar(25) NOT NULL,
  `cantidad` int(11) NOT NULL,
  PRIMARY KEY (`idpublicacioncompra`),
  KEY `producto_publicacionC_PK_idx` (`idproducto`),
  CONSTRAINT `producto_publicacionC_PK` FOREIGN KEY (`idproducto`) REFERENCES `producto` (`idproducto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publicacioncompra`
--

LOCK TABLES `publicacioncompra` WRITE;
/*!40000 ALTER TABLE `publicacioncompra` DISABLE KEYS */;
INSERT INTO `publicacioncompra` VALUES (1,25.9,'ALUA',3),(2,25.6,'ALUA',5),(3,25.3,'ALUA',1),(4,25,'ALUA',2),(5,24.9,'ALUA',5),(6,24.8,'ALUA',2),(7,50.6,'BMA',3),(8,50.5,'BMA',1),(9,50.1,'BMA',5),(10,50,'BMA',2),(11,49.5,'BMA',3),(12,49.4,'BMA',1),(13,33.6,'BYMA',2),(14,33.5,'BYMA',4),(15,33.2,'BYMA',3),(16,33.1,'BYMA',5),(17,33.04,'BYMA',1),(18,32.9,'BYMA',1),(19,95.8,'CEPU',2),(20,95.7,'CEPU',5),(21,95.5,'CEPU',4),(22,95.3,'CEPU',4),(23,95.2,'CEPU',3),(24,95.1,'CEPU',2),(25,160.5,'BTC',1),(26,160.4,'BTC',5),(27,160.3,'BTC',2),(28,160.1,'BTC',3),(29,159.8,'BTC',4),(30,159.7,'BTC',1),(31,84.3,'ETH',2),(32,84.2,'ETH',5),(33,84,'ETH',3),(34,83.9,'ETH',1),(35,83.85,'ETH',5),(36,83.7,'ETH',2),(37,79.6,'XRP',3),(38,79.5,'XRP',1),(39,79.45,'XRP',5),(40,79.3,'XRP',2),(41,79.2,'XRP',3),(42,78.5,'XRP',2);
/*!40000 ALTER TABLE `publicacioncompra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publicacionventa`
--

DROP TABLE IF EXISTS `publicacionventa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publicacionventa` (
  `idpublicacionventa` int(11) NOT NULL,
  `precio` float NOT NULL,
  `idproducto` varchar(20) NOT NULL,
  `cantidad` int(11) NOT NULL,
  PRIMARY KEY (`idpublicacionventa`),
  KEY `producto_publicacionV_Pk_idx` (`idproducto`),
  CONSTRAINT `producto_publicacionV_Pk` FOREIGN KEY (`idproducto`) REFERENCES `producto` (`idproducto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publicacionventa`
--

LOCK TABLES `publicacionventa` WRITE;
/*!40000 ALTER TABLE `publicacionventa` DISABLE KEYS */;
INSERT INTO `publicacionventa` VALUES (1,27.2,'ALUA',2),(2,27.5,'ALUA',3),(3,27.8,'ALUA',1),(4,28.2,'ALUA',4),(5,29,'ALUA',2),(6,29.5,'ALUA',5),(7,51.2,'BMA',2),(8,51.3,'BMA',1),(9,51.6,'BMA',1),(10,51.8,'BMA',3),(11,52,'BMA',2),(12,52.3,'BMA',4),(13,34.2,'BYMA',4),(14,34.4,'BYMA',1),(15,34.8,'BYMA',3),(16,35.2,'BYMA',2),(17,35.5,'BYMA',3),(18,36,'BYMA',1),(19,97,'CEPU',4),(20,97.3,'CEPU',2),(21,97.5,'CEPU',2),(22,97.6,'CEPU',1),(23,97.9,'CEPU',1),(24,98,'CEPU',4),(25,162,'BTC',4),(26,162.5,'BTC',5),(27,162.6,'BTC',2),(28,162.8,'BTC',1),(29,163,'BTC',2),(30,163.2,'BTC',3),(31,85.5,'ETH',1),(32,85.8,'ETH',1),(33,86,'ETH',3),(34,86.5,'ETH',2),(35,86.7,'ETH',3),(36,87,'ETH',4),(37,81.2,'XRP',2),(38,82,'XRP',1),(39,82.2,'XRP',5),(40,82.5,'XRP',3),(41,82.8,'XRP',2),(42,83,'XRP',1);
/*!40000 ALTER TABLE `publicacionventa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suscripcionfondo`
--

DROP TABLE IF EXISTS `suscripcionfondo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suscripcionfondo` (
  `idsuscripcionfondo` int(11) NOT NULL AUTO_INCREMENT,
  `idcliente` int(11) NOT NULL,
  `idfondo` int(11) NOT NULL,
  PRIMARY KEY (`idsuscripcionfondo`),
  KEY `cliente_suscripcion_PK_idx` (`idcliente`),
  KEY `fondo_suscripcion_PK_idx` (`idfondo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suscripcionfondo`
--

LOCK TABLES `suscripcionfondo` WRITE;
/*!40000 ALTER TABLE `suscripcionfondo` DISABLE KEYS */;
/*!40000 ALTER TABLE `suscripcionfondo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaccionactivo`
--

DROP TABLE IF EXISTS `transaccionactivo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaccionactivo` (
  `idtransaccionactivo` int(11) NOT NULL AUTO_INCREMENT,
  `costo` float NOT NULL,
  `cantidad` int(11) NOT NULL,
  `tipo` varchar(45) NOT NULL,
  `comision` float DEFAULT NULL,
  `idproducto` varchar(11) NOT NULL,
  `idcliente` int(11) NOT NULL,
  PRIMARY KEY (`idtransaccionactivo`),
  KEY `producto_transaccion_pk_idx` (`idproducto`),
  KEY `cliente_transaccion_PK_idx` (`idcliente`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaccionactivo`
--

LOCK TABLES `transaccionactivo` WRITE;
/*!40000 ALTER TABLE `transaccionactivo` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaccionactivo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-15 18:25:25
