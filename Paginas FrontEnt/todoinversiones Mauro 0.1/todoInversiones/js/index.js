$(document).ready(function() {
    // Todo lo que esté aca a dentro se ejcuta instantaneamente
    // al terminar de cargarse la página.
    $('#RegistroModal').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text(recipient)
        $.ajax({
                type: 'POST',
                url: 'php/cargarBancos.php'
            })
            .done(function(listas_rep) {
                $('#register-banco').html(listas_rep)
            })
            .fail(function() {
                alert('Hubo un error al cargar los Bancos')
            })
    })

    $('#IngresoModal').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text(recipient)
    })

    $('#register-confirmRegister').on('click', function() {
        var nombre = $('#register-name').val()
        var apellido = $('#register-surname').val()
        var domicilio = $('#register-domicilio').val()
        var dni = $('#register-dni').val()
        var fechaNac = $('#register-fechaNacimiento').val()
        var banco = $('#register-banco').val()
        var nroCuenta = $('#register-numberAccount').val()
        var email = $('#register-email').val()
        var password = $('#register-password').val()
        var terminosDeUso = $('#register-termUse').is(':checked');
        alert(nombre + ' / ' + apellido + ' / ' + domicilio + ' / ' + dni + ' / ' + fechaNac + ' / ' +
            banco + ' / ' + nroCuenta + ' / ' + email + ' / ' + password + ' / ' + terminosDeUso)
        if ((typeof nombre === 'undefined') || (nombre === '') ||
            (typeof apellido === 'undefined') || (apellido === '') ||
            (typeof domicilio === 'undefined') || (domicilio === '') ||
            (typeof dni === 'undefined') || (dni === '') ||
            (typeof fechaNac === 'undefined') || (fechaNac === '') ||
            (typeof banco === 'undefined') || (banco === '') || (banco === '0') ||
            (typeof nroCuenta === 'undefined') || (nroCuenta === '') ||
            (typeof email === 'undefined') || (email === '') ||
            (typeof password === 'undefined') || (password === '') ||
            (!terminosDeUso)) {
            alert('debe completar todos los campos para poder registrarse')
        } else {
            $.ajax({
                    type: 'POST',
                    url: 'php/registrarCliente.php',
                    data: {
                        'nombre': nombre,
                        'apellido': apellido,
                        'domicilio': domicilio,
                        'dni': dni,
                        'fechaNac': fechaNac,
                        'banco': banco,
                        'nroCuenta': nroCuenta,
                        'email': email,
                        'password': password
                    }
                })
                .done(function(listas_rep) {
                    if (listas_rep == true) {
                        alert('el cliente se registro correctamente')
                        $('#RegistroModal').modal('toggle');
                    } else {
                        alert('no se pudo registrar al cliente')
                    }
                })
                .fail(function() {
                    alert('Hubo un error al registrar el cliente')
                })
        }
    })

    $('#loging-confirmIngreso').on('click', function() {
        var dni = $('#loging-dni').val()
        var password = $('#loging-password').val()
        $.ajax({
            type: 'POST',
            url: 'php/ingresar2.php',
            data: {
                'dni': dni,
                'password': password
            },
            cache: "false",
            success: function(data) {
                if (data == "1") {
                    location.href = "php/user.php";

                    alert("aca entro");
                } else {
                    alert('el usuario o la contraseña es incorrecto/a');
                }
            }

        })
    })



})