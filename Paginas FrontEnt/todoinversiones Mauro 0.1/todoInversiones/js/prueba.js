$(document).ready(function() {
    buscarPublicacionCompra('ALUA', 0, '#ALUA-C-1');
    buscarPublicacionCompra('ALUA', 1, '#ALUA-C-2');
    buscarPublicacionCompra('ALUA', 2, '#ALUA-C-3');
    buscarPublicacionCompra('ALUA', 3, '#ALUA-C-4');
    buscarPublicacionCompra('ALUA', 4, '#ALUA-C-5');

    buscarPublicacionCompra('BMA', 0, '#BMA-C-1');
    buscarPublicacionCompra('BMA', 1, '#BMA-C-2');
    buscarPublicacionCompra('BMA', 2, '#BMA-C-3');
    buscarPublicacionCompra('BMA', 3, '#BMA-C-4');
    buscarPublicacionCompra('BMA', 4, '#BMA-C-5');

    buscarPublicacionCompra('BYMA', 0, '#BYMA-C-1');
    buscarPublicacionCompra('BYMA', 1, '#BYMA-C-2');
    buscarPublicacionCompra('BYMA', 2, '#BYMA-C-3');
    buscarPublicacionCompra('BYMA', 3, '#BYMA-C-4');
    buscarPublicacionCompra('BYMA', 4, '#BYMA-C-5');

    buscarPublicacionCompra('CEPU', 0, '#CEPU-C-1');
    buscarPublicacionCompra('CEPU', 1, '#CEPU-C-2');
    buscarPublicacionCompra('CEPU', 2, '#CEPU-C-3');
    buscarPublicacionCompra('CEPU', 3, '#CEPU-C-4');
    buscarPublicacionCompra('CEPU', 4, '#CEPU-C-5');

    buscarPublicacionCompra('BTC', 0, '#BTC-C-1');
    buscarPublicacionCompra('BTC', 1, '#BTC-C-2');
    buscarPublicacionCompra('BTC', 2, '#BTC-C-3');
    buscarPublicacionCompra('BTC', 3, '#BTC-C-4');
    buscarPublicacionCompra('BTC', 4, '#BTC-C-5');

    buscarPublicacionCompra('ETH', 0, '#ETH-C-1');
    buscarPublicacionCompra('ETH', 1, '#ETH-C-2');
    buscarPublicacionCompra('ETH', 2, '#ETH-C-3');
    buscarPublicacionCompra('ETH', 3, '#ETH-C-4');
    buscarPublicacionCompra('ETH', 4, '#ETH-C-5');

    buscarPublicacionCompra('XRP', 0, '#XRP-C-1');
    buscarPublicacionCompra('XRP', 1, '#XRP-C-2');
    buscarPublicacionCompra('XRP', 2, '#XRP-C-3');
    buscarPublicacionCompra('XRP', 3, '#XRP-C-4');
    buscarPublicacionCompra('XRP', 4, '#XRP-C-5');




    buscarPublicacionVenta('ALUA', 0, '#ALUA-V-1');
    buscarPublicacionVenta('ALUA', 1, '#ALUA-V-2');
    buscarPublicacionVenta('ALUA', 2, '#ALUA-V-3');
    buscarPublicacionVenta('ALUA', 3, '#ALUA-V-4');
    buscarPublicacionVenta('ALUA', 4, '#ALUA-V-5');

    buscarPublicacionVenta('BMA', 0, '#BMA-V-1');
    buscarPublicacionVenta('BMA', 1, '#BMA-V-2');
    buscarPublicacionVenta('BMA', 2, '#BMA-V-3');
    buscarPublicacionVenta('BMA', 3, '#BMA-V-4');
    buscarPublicacionVenta('BMA', 4, '#BMA-V-5');

    buscarPublicacionVenta('BYMA', 0, '#BYMA-V-1');
    buscarPublicacionVenta('BYMA', 1, '#BYMA-V-2');
    buscarPublicacionVenta('BYMA', 2, '#BYMA-V-3');
    buscarPublicacionVenta('BYMA', 3, '#BYMA-V-4');
    buscarPublicacionVenta('BYMA', 4, '#BYMA-V-5');

    buscarPublicacionVenta('CEPU', 0, '#CEPU-V-1');
    buscarPublicacionVenta('CEPU', 1, '#CEPU-V-2');
    buscarPublicacionVenta('CEPU', 2, '#CEPU-V-3');
    buscarPublicacionVenta('CEPU', 3, '#CEPU-V-4');
    buscarPublicacionVenta('CEPU', 4, '#CEPU-V-5');

    buscarPublicacionVenta('BTC', 0, '#BTC-V-1');
    buscarPublicacionVenta('BTC', 1, '#BTC-V-2');
    buscarPublicacionVenta('BTC', 2, '#BTC-V-3');
    buscarPublicacionVenta('BTC', 3, '#BTC-V-4');
    buscarPublicacionVenta('BTC', 4, '#BTC-V-5');

    buscarPublicacionVenta('ETH', 0, '#ETH-V-1');
    buscarPublicacionVenta('ETH', 1, '#ETH-V-2');
    buscarPublicacionVenta('ETH', 2, '#ETH-V-3');
    buscarPublicacionVenta('ETH', 3, '#ETH-V-4');
    buscarPublicacionVenta('ETH', 4, '#ETH-V-5');

    buscarPublicacionVenta('XRP', 0, '#XRP-V-1');
    buscarPublicacionVenta('XRP', 1, '#XRP-V-2');
    buscarPublicacionVenta('XRP', 2, '#XRP-V-3');
    buscarPublicacionVenta('XRP', 3, '#XRP-V-4');
    buscarPublicacionVenta('XRP', 4, '#XRP-V-5');

    $('#VentaModal').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var activo = button.data('activo') // Extract info from data-* attributes
        var posicion = parseInt(button.data('posicion'), 10)
        buscarPrecioPublicacionVenta(activo, posicion)
        buscarCantidadPublicacionVenta(activo, posicion)
        calcularTotalVenta(activo, posicion)
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('#tipoActivoV').text(activo)
    })

    $('#CompraModal').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var activo = button.data('activo') // Extract info from data-* attributes
        var posicion = parseInt(button.data('posicion'), 10)
        buscarPrecioPublicacionCompra(activo, posicion)
        buscarCantidadPublicacionCompra(activo, posicion)
        calcularTotalCompra(activo, posicion)
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('#tipoActivoC').text(activo)
    })


})

function buscarPublicacionCompra(producto, posicion, idfila) {

    var productoi = producto;
    var posicioni = posicion;
    $.ajax({

            type: 'POST',
            url: 'php/cargarPubliacionesCompra.php',
            data: {
                'producto': productoi,
                'posicion': posicioni
            }
        })
        .done(function(response) {
            $(idfila).append(response)
        })
        .fail(function() {


        });
};


function buscarPublicacionVenta(producto, posicion, idfila) {

    var productoi = producto;
    var posicioni = posicion;
    $.ajax({

            type: 'POST',
            url: 'php/cargarPublicacionesVenta.php',
            data: {
                'producto': productoi,
                'posicion': posicioni
            }
        })
        .done(function(response) {
            $(idfila).append(response)
        })
        .fail(function() {


        });
};

function buscarCantidadPublicacionVenta(producto, posicion) {

    var productoi = producto;
    var posicioni = posicion;
    $.ajax({

            type: 'POST',
            url: 'php/buscarCantidadPublicacionVenta.php',
            data: {
                'producto': productoi,
                'posicion': posicioni
            }
        })
        .done(function(response) {
            $('#cantidadActivoV').html(response);
        })
        .fail(function() {


        });
};

function buscarPrecioPublicacionVenta(producto, posicion) {

    var productoi = producto;
    var posicioni = posicion;

    $.ajax({

            type: 'POST',
            url: 'php/buscarPrecioPublicacionVenta.php',
            data: {
                'producto': productoi,
                'posicion': posicioni
            }
        })
        .done(function(response) {
            $('#precioActivoV').html(response);
        })
        .fail(function() {


        });
};

function calcularTotalVenta(producto, posicion) {

    var productoi = producto;
    var posicioni = posicion;

    $.ajax({

            type: 'POST',
            url: 'php/calcularTotalVenta.php',
            data: {
                'producto': productoi,
                'posicion': posicioni
            }
        })
        .done(function(response) {
            $('#totalActivoV').html(response);
        })
        .fail(function() {


        });
};

function buscarCantidadPublicacionCompra(producto, posicion) {

    var productoi = producto;
    var posicioni = posicion;
    $.ajax({

            type: 'POST',
            url: 'php/buscarCantidadPublicacionCompra.php',
            data: {
                'producto': productoi,
                'posicion': posicioni
            }
        })
        .done(function(response) {
            $('#cantidadActivoC').html(response);
        })
        .fail(function() {


        });
};

function buscarPrecioPublicacionCompra(producto, posicion) {

    var productoi = producto;
    var posicioni = posicion;

    $.ajax({

            type: 'POST',
            url: 'php/buscarPrecioPublicacionCompra.php',
            data: {
                'producto': productoi,
                'posicion': posicioni
            }
        })
        .done(function(response) {
            $('#precioActivoC').html(response);
        })
        .fail(function() {


        });
};

function calcularTotalCompra(producto, posicion) {

    var productoi = producto;
    var posicioni = posicion;

    $.ajax({

            type: 'POST',
            url: 'php/calcularTotalCompra.php',
            data: {
                'producto': productoi,
                'posicion': posicioni
            }
        })
        .done(function(response) {
            $('#totalActivoC').html(response);
        })
        .fail(function() {


        });
};