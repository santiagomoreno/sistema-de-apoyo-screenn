<?php
session_start();
if(isset($_SESSION['idCliente'])&&isset($_SESSION['nombre'])&&isset($_SESSION['apellido'])&&isset($_SESSION['domicilio'])&&isset($_SESSION['dni'])&&isset($_SESSION['fechaNacimiento'])&&isset($_SESSION['email'])&&isset($_SESSION['fechaIngreso'])&&isset($_SESSION['cuentaComitente'])){
  header("location:php/user.php");
}elseif(isset($_SESSION['idCliente'])||isset($_SESSION['nombre'])||isset($_SESSION['apellido'])||isset($_SESSION['domicilio'])||isset($_SESSION['dni'])||isset($_SESSION['fechaNacimiento'])||isset($_SESSION['email'])||isset($_SESSION['fechaIngreso'])||isset($_SESSION['cuentaComitente'])){
  header("location:php/cerrarSession.php");
}
?>

<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <title>Todo Inversiones</title>
    <link rel="shortcut icon" href="imagenes/manos.png">
</head>

<body  >
    <!--
  <div class="container">
    <div class="page-header text-left">-->
    <input type="hidden" id="usuarioLogeado" value="false">

    <!-- Inicio: Encabezado -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href=""><img src="imagenes/iconoTodoInversioness2.png" alt="" /> TodoInversiones.com</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" id="portafolio">Portafolio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="cerrarSesion">Cerrar Sesion</a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- Logo -->
    <!--<div id="bp-logo"><img src="imagenes/iconoTodoInversioness2.png" alt=""/>TodoInversiones.com</div>-->
    <!-- Fin: Encabezado -->

    <div class="container">

        <h2>Elija el tipo de inversion que desea realizar:</h2>
        <br>
        <!-- tabs categorias de productos -->
        <ul class="nav nav-pills" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="pill" href="#acciones">Acciones</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="pill" href="#bonos">Bonos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="pill" href="#cripto">Criptomonedas</a>
            </li>
        </ul>

        <!-- contenido de productos -->
        <div class="tab-content">
            <!-- tabs acciones -->
            <div id="acciones" class="container tab-pane active in"><br>
                <div class="tabbable">
                    <ul class="nav nav-pills" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="pill" href="#ALUA">ALUA</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="pill" href="#BMA">BMA</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="pill" href="#BYMA">BYMA</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="pill" href="#CEPU">CEPU</a>
                        </li>
                    </ul>
                    <br>
                    <div class="tab-content">
                        <div id="ALUA" class="container tab-pane active in">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th colspan="12" class="fontsize20">Aluar Compra</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>Cantidad Compra</td>
                                        <td>Precio Compra</td>
                                        

                                    </tr>
                                    <tr id="ALUA-C-1" >

                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="ALUA"data-tipo="C"data-posicion="0">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="ALUA-C-2">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="ALUA"data-tipo="C"data-posicion="1">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="ALUA-C-3">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="ALUA"data-tipo="C"data-posicion="2">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="ALUA-C-4">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="ALUA"data-tipo="C"data-posicion="3">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="ALUA-C-5">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="ALUA"data-tipo="C"data-posicion="4">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                </tbody>

                            </table>
                            <br>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th colspan="12" class="fontsize20">Aluar Venta</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>Cantidad Venta</td>
                                        <td>Precio Venta</td>
                                        

                                    </tr>
                                    <tr id="ALUA-V-1">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="ALUA"data-tipo="C"data-posicion="0">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="ALUA-V-2">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="ALUA"data-tipo="C"data-posicion="1">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="ALUA-V-3">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="ALUA"data-tipo="C"data-posicion="2">Comprar</button></a></strong>
                                        </td>


                                    </tr>
                                    <tr id="ALUA-V-4">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="ALUA"data-tipo="C"data-posicion="3">Comprar</button></a></strong>
                                        </td>


                                    </tr>
                                    <tr id="ALUA-V-5">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="ALUA"data-tipo="C"data-posicion="4">Comprar</button></a></strong>
                                        </td>


                                    </tr>
                                </tbody>

                            </table>

                        </div>
                        <div id="BMA" class="container tab-pane fade">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th colspan="12" class="fontsize20">Banco Macro Compra</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>Cantidad Compra</td>
                                        <td>Precio Compra</td>

                                    </tr>
                                    <tr id="BMA-C-1">
                                       
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="BMA"data-tipo="C"data-posicion="0">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="BMA-C-2">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="BMA"data-tipo="C"data-posicion="1">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="BMA-C-3">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="BMA"data-tipo="C"data-posicion="2">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="BMA-C-4">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="BMA"data-tipo="C"data-posicion="3">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="BMA-C-5">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="BMA"data-tipo="C"data-posicion="4">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                </tbody>

                            </table>
                            <br>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th colspan="12" class="fontsize20">Banco Macro Venta</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>Cantidad Venta</td>
                                        <td>Precio Venta</td>
                                        

                                    </tr>
                                    <tr id="BMA-V-1">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="BMA"data-tipo="C"data-posicion="0">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="BMA-V-2">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="BMA"data-tipo="C"data-posicion="1">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="BMA-V-3">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="BMA"data-tipo="C"data-posicion="2">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="BMA-V-4">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="BMA"data-tipo="C"data-posicion="3">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="BMA-V-5">
                                       
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="BMA"data-tipo="C"data-posicion="4">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                </tbody>

                            </table>

                        </div>
                        <div id="BYMA" class="container tab-pane fade">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th colspan="12" class="fontsize20">Bolsas Y Mercados Argentinos S.A. Compra</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>Cantidad Compra</td>
                                        <td>Precio Compra</td>
                                        

                                    </tr>
                                    <tr id="BYMA-C-1">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="BYMA"data-tipo="C"data-posicion="0">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="BYMA-C-2">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="BYMA"data-tipo="C"data-posicion="1">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="BYMA-C-3">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="BYMA"data-tipo="C"data-posicion="2">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="BYMA-C-4">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="BYMA"data-tipo="C"data-posicion="3">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="BYMA-C-5">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="BYMA"data-tipo="C"data-posicion="4">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                </tbody>

                            </table>
                            <br>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th colspan="12" class="fontsize20">Bolsas Y Mercados Argentinos S.A. Venta</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>Cantidad Venta</td>
                                        <td>Precio Venta</td>
                                        

                                    </tr>
                                    <tr id="BYMA-V-1">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="BYMA"data-tipo="C"data-posicion="0">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="BYMA-V-2">
                                       
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="BYMA"data-tipo="C"data-posicion="1">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="BYMA-V-3">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="BYMA"data-tipo="C"data-posicion="2">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="BYMA-V-4">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="BYMA"data-tipo="C"data-posicion="3">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="BYMA-V-5">
                                       
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="BYMA"data-tipo="C"data-posicion="4">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                </tbody>

                            </table>

                        </div>
                        <div id="CEPU" class="container tab-pane fade">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th colspan="12" class="fontsize20">Central Puerto S.A. Compra</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>Cantidad Compra</td>
                                        <td>Precio Compra</td>
                                        

                                    </tr>
                                    <tr id="CEPU-C-1">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="CEPU"data-tipo="C"data-posicion="0">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="CEPU-C-2">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="CEPU"data-tipo="C"data-posicion="1">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="CEPU-C-3">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="CEPU"data-tipo="C"data-posicion="2">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="CEPU-C-4">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="CEPU"data-tipo="C"data-posicion="3">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="CEPU-C-5">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="CEPU"data-tipo="C"data-posicion="4">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                </tbody>

                            </table>
                            <br>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th colspan="12" class="fontsize20">Central Puerto S.A. Venta</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>Cantidad Venta</td>
                                        <td>Precio Venta</td>
                                        

                                    </tr>
                                    <tr id="CEPU-V-1">
                                      
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="CEPU"data-tipo="C"data-posicion="0">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="CEPU-V-2">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="CEPU"data-tipo="C"data-posicion="1">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="CEPU-V-3">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="CEPU"data-tipo="C"data-posicion="2">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="CEPU-V-4">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="CEPU"data-tipo="C"data-posicion="3">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="CEPU-V-5">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="CEPU"data-tipo="C"data-posicion="4">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                </tbody>

                            </table>

                        </div>
                    </div>
                </div>

            </div>
            <!-- tabs bonos -->
            <br>
            <div id="bonos" class="container tab-pane fade"><br>
                <h3>Aca iria la lista de bonos</h3>
                <br>
            </div>
            <!-- tabs criptomonedas -->
            <div id="cripto" class="container tab-pane fade">
                <div class="tabbable">
                    <ul class="nav nav-pills" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="pill" href="#BTC">BTC</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="pill" href="#ETH">ETH</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="pill" href="#XRP">XRP</a>
                        </li>

                    </ul>
                    <br>
                    <div class="tab-content">

                        <div id="BTC" class="container tab-pane active in">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th colspan="12" class="fontsize20">BitCoin Compra</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>Cantidad Compra</td>
                                        <td>Precio Compra</td>
                                        

                                    </tr>
                                    <tr id="BTC-C-1">
                                       
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="BTC"data-tipo="C"data-posicion="0">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="BTC-C-2">
                                       
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="BTC"data-tipo="C"data-posicion="1">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="BTC-C-3">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="BTC"data-tipo="C"data-posicion="2">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="BTC-C-4">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="BTC"data-tipo="C"data-posicion="3">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="BTC-C-5">
                                       
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="BTC"data-tipo="C"data-posicion="4">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                </tbody>

                            </table>
                            <br>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th colspan="12" class="fontsize20">BitCoin Venta</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>Cantidad Venta</td>
                                        <td>Precio Venta</td>
                                        

                                    </tr>
                                    <tr id="BTC-V-1">
                                       
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="BTC"data-tipo="C"data-posicion="0">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="BTC-V-2">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="BTC"data-tipo="C"data-posicion="1">Comprar</button></a></strong>
                                        </td>
                                    </tr>
                                    <tr id="BTC-V-3">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="BTC"data-tipo="C"data-posicion="2">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="BTC-V-4">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="BTC"data-tipo="C"data-posicion="3">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="BTC-V-5">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="BTC"data-tipo="C"data-posicion="4">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                </tbody>

                            </table>


                        </div>
                        <div id="ETH" class="container tab-pane fade">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th colspan="12" class="fontsize20">Ethereum Compra</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>Cantidad Compra</td>
                                        <td>Precio Compra</td>
                                        

                                    </tr>
                                    <tr id="ETH-C-1">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="ETH"data-tipo="C"data-posicion="0">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="ETH-C-2">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="ETH"data-tipo="C"data-posicion="1">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="ETH-C-3">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="ETH"data-tipo="C"data-posicion="2">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="ETH-C-4">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="ETH"data-tipo="C"data-posicion="3">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="ETH-C-5">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="ETH"data-tipo="C"data-posicion="4">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                </tbody>

                            </table>
                            <br>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th colspan="12" class="fontsize20">Ethereum Venta</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>Cantidad Venta</td>
                                        <td>Precio Venta</td>
                                        

                                    </tr>
                                    <tr id="ETH-V-1">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="ETH"data-tipo="C"data-posicion="0">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="ETH-V-2">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="ETH"data-tipo="C"data-posicion="1">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="ETH-V-3">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="ETH"data-tipo="C"data-posicion="2">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="ETH-V-4">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="ETH"data-tipo="C"data-posicion="3">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="ETH-V-5">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="ETH"data-tipo="C"data-posicion="4">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                </tbody>

                            </table>

                        </div>
                        <div id="XRP" class="container tab-pane fade">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th colspan="12" class="fontsize20">Ripple Compra</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>Cantidad Compra</td>
                                        <td>Precio Compra</td>
                                        

                                    </tr>
                                    <tr id="XRP-C-1">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="XRP"data-tipo="C"data-posicion="0">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="XRP-C-2">
                                       
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="XRP"data-tipo="C"data-posicion="1">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="XRP-C-3">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="XRP"data-tipo="C"data-posicion="2">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="XRP-C-4">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="XRP"data-tipo="C"data-posicion="3">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="XRP-C-5">
                                        
                                    <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#CompraModal" data-activo="XRP"data-tipo="C"data-posicion="4">Vender</button></a></strong>
                                        </td>

                                    </tr>
                                </tbody>

                            </table>
                            <br>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th colspan="12" class="fontsize20">Ripple Venta</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>Cantidad Venta</td>
                                        <td>Precio Venta</td>
                                        

                                    </tr>
                                    <tr id="XRP-V-1">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="XRP"data-tipo="C"data-posicion="0">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="XRP-V-2">
                                       
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="XRP"data-tipo="C"data-posicion="1">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="XRP-V-3">
                                      
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="XRP"data-tipo="C"data-posicion="2">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="XRP-V-4">
                                        
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="XRP"data-tipo="C"data-posicion="3">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                    <tr id="XRP-V-5">
                                       
                                        <td>
                                            <strong><a class="nav-link"><button id="botonRegistro" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#VentaModal" data-activo="XRP"data-tipo="C"data-posicion="4">Comprar</button></a></strong>
                                        </td>

                                    </tr>
                                </tbody>

                            </table>


                        </div>
                        <br>
                    </div>
                </div>
            </div>


        </div>


        <!-- Ventana modal confirmar transaccion Venta-->
        <div class="modal fade" id="VentaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Compra</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="loging-dni" class="col-form-label">Accion:</label>
                                <label for="loging-dni" class="col-form-label" id="tipoActivoV">Accion</label>
                            </div>
                            <div class="form-group">
                                <label for="loging-dni" class="col-form-label">Cantidad:</label>
                                <label for="loging-dni" class="col-form-label" id="cantidadActivoV">Cantidad</label>
                            </div>
                            <div class="form-group">
                                <label for="loging-dni" class="col-form-label">Precio:</label>
                                <label for="loging-dni" class="col-form-label" id="precioActivoV">Cantidad</label>
                            </div>
                            <div class="form-group">
                                <label for="loging-dni" class="col-form-label">Comision:</label>
                                <label for="loging-dni" class="col-form-label" ><strong>AR$ 100</strong></label>
                            </div>
                            <div class="form-group">
                                <label for="loging-dni" class="col-form-label">Total:</label>
                                <label for="loging-dni" class="col-form-label" id="totalActivoV">Total</label>
                            </div>
                            <div class="form-group">
                                <label for="loging-dni" class="col-form-label">Dinero disponible:</label>
                                <label for="loging-dni" class="col-form-label" id="dineroClienteV">Total</label>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="loging-confirmIngreso">Comprar</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="loging-cancelIngreso">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>


        <!-- Ventana modal confirmar transaccion Compra-->
        <div class="modal fade" id="CompraModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Venta</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="loging-dni" class="col-form-label">Accion:</label>
                                <label for="loging-dni" class="col-form-label" id="tipoActivoC">Accion</label>
                            </div>
                            <div class="form-group">
                                <label for="loging-dni" class="col-form-label">Cantidad:</label>
                                <label for="loging-dni" class="col-form-label" id="cantidadActivoC">Cantidad</label>
                            </div>
                            <div class="form-group">
                                <label for="loging-dni" class="col-form-label">Precio:</label>
                                <label for="loging-dni" class="col-form-label" id="precioActivoC">Cantidad</label>
                            </div>
                            
                            <div class="form-group">
                                <label for="loging-dni" class="col-form-label">Total:</label>
                                <label for="loging-dni" class="col-form-label" id="totalActivoC">Total</label>
                            </div>
                            <div class="form-group">
                                <label for="loging-dni" class="col-form-label">Acciones disponibles:</label>
                                <label for="loging-dni" class="col-form-label" id="dineroClienteC">Total</label>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="loging-confirmIngreso">Vender</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="loging-cancelIngreso">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>


    </div>






    <!-- Inicio: Pie de Pagina -->
    <footer id="pieDePagina">
        <div class="bp-wraper">
            <div class="bp-col-2">
                <p>InvertirOnline 0810-1582-IOL&#40;465&#41;</p>
                <p>Juan María Gutiérrez 1150, B1613 Los Polvorines, Buenos Aires</p>
            </div>
            <div class="bp-col-2 bp-right">
                <p>
                    <span>
                <a href="https://www.facebook.com" title="Facebook" target="_blank">
                  <img src="https://s3.amazonaws.com/resources.invertironline.com/external/productos/images/facebook.png" alt=""/>
                </a>
              </span>
                    <span>
                <a href="https://twitter.com" title="Twitter" target="_blank">
                  <img src="https://s3.amazonaws.com/resources.invertironline.com/external/productos/images/twitter.png" alt=""/>
                </a>
              </span>
                </p>
                <p>Designed by <a title="sample text" target="_blank">Sample Text</a></p>
            </div>
        </div>
    </footer>
    <!-- Fin: Pie de Pagina -->
























    <!--   </div>    
  </div>-->
    <!-- Aqui se carga la libreria jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js">
    </script>
    <!-- Aqui indicamos donde esta nuetro código java Script -->
    <script type="text/javascript" src="js/prueba.js"></script>
    <link rel="StyleSheet" href="css/estiloTodoInversiones.css" type="text/css">

    <script type="text/javascript" src="bootstrap/jsBootstrap/bootstrap.min.js"></script>
    <link rel="StyleSheet" href="bootstrap/cssBootstrap/bootstrap.css" type="text/css">
    <link rel="StyleSheet" href="bootstrap/cssBootstrap/bootstrap.min.css" type="text/css">
    <!--
  <link rel="StyleSheet" href="bootstrap/bootstrap-grid.css" type="text/css">
  <link rel="StyleSheet" href="bootstrap/bootstrap-grid.min.css" type="text/css">
  <link rel="StyleSheet" href="bootstrap/bootstrap-reboot.css" type="text/css">
  <link rel="StyleSheet" href="bootstrap/bootstrap-reboot.min.css" type="text/css">-->

</body>

</html>