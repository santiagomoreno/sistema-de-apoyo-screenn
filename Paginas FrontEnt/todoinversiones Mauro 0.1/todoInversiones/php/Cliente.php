<?php
require_once 'conexion.php';

 class Cliente implements JsonSerializable{
   private $id;
   private $nombre;
   private $apellido;
   private $domicilio;
   private $dni;
   private $fechaNac;
   private $email;
   private $password;
   private $fechaIngreso;
   private $cuentaComitente;
   const TABLA = 'cliente';

   public function __construct($nombre, $apellido, $domicilio, $dni, $fechaNac, $email, $password, $fechaIngreso, $cuentaComitente, $id) {
      $this->nombre = $nombre;
      $this->apellido = $apellido;
      $this->domicilio = $domicilio;
      $this->dni = $dni;
      $this->fechaNac = $fechaNac;
      $this->email = $email;
      $this->password = $password;
      $this->fechaIngreso = $fechaIngreso;
      $this->cuentaComitente = $cuentaComitente;
      $this->id = $id;
   }
   public function getId() {
      return $this->id;
   }
   public function getNombre() {
      return $this->nombre;
   }
   public function getApellido() {
      return $this->apellido;
   }
   public function getDomicilio() {
      return $this->domicilio;
   }
   public function getDni() {
      return $this->dni;
   }
   public function getFechaNac() {
      return $this->fechaNac;
   }
   public function getEmail() {
      return $this->email;
   }
   public function getPassword() {
      return $this->password;
   }
   public function getFechaIngreso() {
      return $this->fechaIngreso;
   }
   public function getCuentaComitente() {
      return $this->cuentaComitente;
   }
   public function setNombre($nombre) {
      $this->nombre = $nombre;
   }
   public function setApellido($apellido) {
      $this->apellido = $apellido;
   }
   public function setDomicilio($domicilio) {
      $this->domicilio = $domicilio;
   }
   public function setDni($dni) {
      $this->dni = $dni;
   }
   public function setFechaNac($fechaNac) {
      $this->fechaNac = $fechaNac;
   }
   public function setEmail($email) {
      $this->email = $email;
   }
   public function setPassword($password) {
      $this->password = $password;
   }
   public function setFechaIngreso($fechaIngreso) {
      $this->fechaIngreso = $fechaIngreso;
   }
   public function setCuentaComitente($cuentaComitente) {
      $this->cuentaComitente = $cuentaComitente;
   }
   public function guardar(){
      $conexion = new conexion();
      if($this->id) /*Modifica*/ {
         $consulta = $conexion->prepare('UPDATE ' . self::TABLA .' SET nombre = :nombre, apellido = :apellido, domicilio = :domicilio, dni = :dni, fechaNac = :fechaNac, email = :email, password = :password, apellido = :apellido, apellido = :apellido WHERE id = :id');
         $consulta->bindParam(':nombre', $this->nombre);
         $consulta->bindParam(':descripcion', $this->descripcion);
         $consulta->bindParam(':id', $this->id);
         $consulta->execute();
      }else /*Inserta*/ {
         $consulta = $conexion->prepare('INSERT INTO ' . self::TABLA .' (nombre, descripcion) VALUES(:nombre, :descripcion)');
         $consulta->bindParam(':nombre', $this->nombre);
         $consulta->bindParam(':descripcion', $this->descripcion);
         $consulta->execute();
         $this->id = $conexion->lastInsertId();
      }
      $conexion = null;
   }
   public function guardar2(){
      $consulta = "INSERT INTO ". self::TABLA . "(nombre, apellido, domicilio, dni, fechaNacimiento, email, password, fechaIngreso, cuentaComitente) VALUES ('$nombre', '$apellido','$domicilio','$dni','$fechaNac','$email','$password','$fechaIngreso','$cuentaComitente');";
   }

   public function jsonSerialize() { 
      return [ 'nombre' => $this->nombre, 'apellido' => $this->apellido, 'domicilio' => $this->domicilio, 'dni' => $this->dni, 'fechaNac' => $this->fechaNac, 'email' => $this->email, 'password' => $this->password, 'fechaIngreso' => $this->fechaIngreso, 'cuentaComitente' => $this->cuentaComitente ];
   } 
 }
?>