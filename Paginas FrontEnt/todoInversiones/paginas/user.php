<?php
require_once '../php/Cliente.php';
session_start();

if((!isset($_SESSION['cliente']))){
  header("location:../index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Todo Inversiones</title>
  <link rel="shortcut icon" href="../imagenes/manos.png">
</head>
<body><!--
  <div class="container">
    <div class="page-header text-left">-->
       <input type="hidden" id="usuarioLogeado" value="false">

      <!-- Inicio: Encabezado -->      
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href=""><img src="../imagenes/iconoTodoInversioness2.png" alt=""/> TodoInversiones.com</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" id="perfil"><?php echo $_SESSION['cliente']->getNombre(); ?></a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" id="invertir">Ofertas de Invercion</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" id="portafolio">Portafolio</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" id="encuesta">Encuesta Satisfaccion</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="cerrarSesion" href="../php/cerrarSession.php">Cerrar Sesion</a>
            </li>
          </ul>          
        </div>
      </nav>
      <!-- Fin: Encabezado -->

      <!-- Inicio: Presentacion -->
      <div id="datosUsuario" >
        <h1>Bienvenido:<p id="nombreUserCliente"><?php echo /*$_SESSION['idCliente']*/$_SESSION['cliente']->getId(); echo "<br>"; echo /*$_SESSION['nombre']*/$_SESSION['cliente']->getNombre();echo "<br>"; echo /*$_SESSION['apellido']*/$_SESSION['cliente']->getApellido();echo "<br>"; echo /*$_SESSION['domicilio']*/$_SESSION['cliente']->getDomicilio();echo "<br>"; echo /*$_SESSION['dni']*/$_SESSION['cliente']->getDni();echo "<br>"; echo /*$_SESSION['fechaNacimiento']*/$_SESSION['cliente']->getFechaNac();echo "<br>"; echo /*$_SESSION['email']*/$_SESSION['cliente']->getEmail();echo "<br>"; echo /*$_SESSION['fechaIngreso']*/$_SESSION['cliente']->getFechaIngreso();echo "<br>"; echo /*$_SESSION['cuentaComitente']*/$_SESSION['cliente']->getCuentaComitente();echo "<br>";echo $_SESSION['cliente']->getPerfilDeInversor(); ?><button id="RealizarTestInversor2" type="button" class="btn btn-primary btn-block">Realizar Test Inversor</button></p></h1>

      </div>
      <!-- Fin: Presentacion -->



<!-- Inicio: Presentacion -->
      <div id="presentacion" style="display: none;">
        <h1>¿C&oacute;mo hacer para que tus ahorros crezcan? F&aacute;cil, <span>invertilos</span></h1>

        <p>Somos un broker especializado en trading online. Integramos informaci&oacute;n financiera actualizada, asesoramiento especializado y administraci&oacute;n de inversiones.</p>
        
        <!-- <img src="imagenes/pensandoInvertir - copia.png" alt=""/> -->
      </div>
      <!-- Fin: Presentacion -->

      <!-- Inicio: Perfil del Inversor -->
      <section id="EjemplosPerfilesDeInversion" style="display: none;">
        <div class="bp-wraper-min">
          <div class="apartadosDeTitulos">
            <h2>Oper&aacute; seg&uacute;n tu perfil de inversor&#58;</h2>
          </div>
          <div class="bp-full-w" style="font-size: 18px;">
          <article class="apartadosArticulos">
            <figure>
              <img src="../imagenes/perfilConservador.png" alt=""/>
              <figcaption><strong style="font-size: 20px;">Perfil Conservador&#58;</strong></figcaption>
              <ul style="text-align: left; padding-left: 50px;">
                <li>&#8226; Bonos Locales</li>           
                <li>&#8226; Fondos Comunes Conservadores</li>    
              </ul>
            </figure>
          </article>
          <article class="apartadosArticulos">
            <figure>
              <img src="../imagenes/perfilModerado.png" alt=""/>
              <figcaption><strong style="font-size: 20px;">Perfil Moderado&#58;</strong></figcaption>
              <ul style="text-align: left; padding-left: 67px;">
                <li>&#8226; Bonos Locales</li>            
                <li>&#8226; Fondos Comunes Moderados</li>
                <li>&#8226; Algunas Acciones</li>
              </ul>
            </figure>
          </article>
          <article class="apartadosArticulos">
            <figure>
              <img src="../imagenes/perfilAgresivo.png" alt=""/>
              <figcaption><strong style="font-size: 20px;">Perfil Agresivo&#58;</strong></figcaption>
              <ul style="text-align: left; padding-left: 73px;">
                <li>&#8226; Todo</li>    
              </ul>
            </figure>
          </article>
          </div>
        </div>
      </section>
      <!-- Fin: Perfil del Inversor -->



      <!-- Inicio: Seccion Beneficios -->
      <section id="apartadosBeneficios" style="display: none;">
        <div class="bp-wraper-min">
          <div class="apartadosDeTitulos">
            <h2>¡Conoc&eacute; los beneficios que te esperan&#33;</h2>
          </div>
          <div class="bp-full-w">
            <figure>
              <img src="../imagenes/beneficioTransferirInversion.png" alt=""/>
              <figcaption>Podes transferir tus inversiones de otras aplicaciones sin costo alguno, y as&iacute; almacenarlas en un solo lugar para su comodidad.</figcaption>
            </figure>
            <figure>
              <img src="../imagenes/beneficioAsesoramientoFinanciero.png" alt=""/>
              <figcaption>Ten&eacute;s acceso a asesoramiento financiero.</figcaption>
            </figure>
            <figure>
              <img src="../imagenes/beneficioCotizacion.png" alt=""/>
              <figcaption>Cotizaciones en tiempo real disponibles.</figcaption>
            </figure>
            <figure>
              <img src="../imagenes/beneficioMultiplataforma.png" alt=""/>
              <figcaption>Oper&aacute;s 100&#37; online desde tu PC o Celular.</figcaption>
            </figure>
          </div>
        </div>  
      </section>
      <!-- Fin: Seccion Beneficios -->



      <!--Inicio: Ventana Modal Advertencia Perfil Inversor-->
      <div class="modal fade" id="MensajeAdvertenciaPerfilInversor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">New message</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form>
                <div class="form-group">
                  <label for="loging-dni" class="col-form-label">No se permite acceder a las ofertas de inversion hasta que se realize el test de perfil de inversion.</label>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-success" id="realizarTestDeInversion">Realizar Test de Perfil de Inversion</button>
            </div>
          </div>
        </div>
      </div>
      </div>
      <!--Fin: Ventana Modal Advertencia Perfil Inversor-->






      <!-- Inicio: Pie de Pagina -->
      <footer id="pieDePagina">
        <div class="bp-wraper"> 
          <div class="bp-col-2">
            <p>InvertirOnline 0810-1582-IOL&#40;465&#41;</p>
            <p>Juan María Gutiérrez 1150, B1613 Los Polvorines, Buenos Aires</p>
          </div>
          <div class="bp-col-2 bp-right">
            <p>
              <span>
                <a href="https://www.facebook.com" title="Facebook" target="_blank">
                  <img src="https://s3.amazonaws.com/resources.invertironline.com/external/productos/images/facebook.png" alt=""/>
                </a>
              </span>
              <span>
                <a href="https://twitter.com" title="Twitter" target="_blank">
                  <img src="https://s3.amazonaws.com/resources.invertironline.com/external/productos/images/twitter.png" alt=""/>
                </a>
              </span>     
            </p>
            <p>Designed by <a title="sample text" target="_blank">Sample Text</a></p>
          </div>
        </div>
      </footer>
      <!-- Fin: Pie de Pagina -->












  <!-- Aqui se carga la libreria jquery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js">
  </script>
  <!-- Aqui indicamos donde esta nuetro código java Script -->
  <script type="text/javascript" src="../js/user.js"></script>
  <link rel="StyleSheet" href="../css/estiloTodoInversiones.css" type="text/css">

  <script type="text/javascript" src="../bootstrap/jsBootstrap/bootstrap.min.js"></script>
  <link rel="StyleSheet" href="../bootstrap/cssBootstrap/bootstrap.css" type="text/css">
  <link rel="StyleSheet" href="../bootstrap/cssBootstrap/bootstrap.min.css" type="text/css">

</body>
</html>