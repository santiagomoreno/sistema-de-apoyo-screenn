<?php
require_once '../php/Cliente.php';
session_start();

if((!isset($_SESSION['cliente']))){
  header("location:../index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Todo Inversiones</title>
  <link rel="shortcut icon" href="../imagenes/manos.png">
</head>
<body>

	<!-- Inicio: Encabezado -->      
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href=""><img src="../imagenes/iconoTodoInversioness2.png" alt=""/> TodoInversiones.com</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" id="perfil"><?php echo $_SESSION['cliente']->getNombre(); ?></a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" id="invertir">Ofertas de Invercion</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" id="portafolio">Portafolio</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" id="encuesta">Encuesta Satisfaccion</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="cerrarSesion" href="../php/cerrarSession.php">Cerrar Sesion</a>
            </li>
          </ul>          
        </div>
      </nav>
      <!-- Fin: Encabezado -->

      <!-- Inicio: Encuesta Satisfaccion -->
      <div id="EncuestaSatisfaccion">
        <h1>Test Perfil Inversor</h1>
        <h3>1) <a id="pregunta1">¿Te sentis comodo con la aplicacion?</a></h3> <!--<span>invertilos</span></h3>-->
        <br>
        <p><span>A)</span><input type="radio" class="radioTest" name="respuesta1" data-whatever="Si" value="2"/><a id=respuesta1p1>Si</a></p> 
        <p><span>B) </span><input type="radio" class="radioTestB" name="respuesta1" data-whatever="Mas o menos" value="1"/><a id=respuesta2p1>Mas o menos</a></p> 
        <p><span>C)</span><input type="radio" class="radioTest" name="respuesta1" data-whatever="No" value="0"/><a id=respuesta3p1>No</a></p> 
        <br>
        <h3>2) <a id="pregunta2">¿Te parece facil utilizar la aplicacion?</a></h3>
        <br>
        <p><span>A)</span><input type="radio" class="radioTest" name="respuesta2" data-whatever="Si" value=2/><a id=respuesta1p2>Si</a></p> 
        <p><span>B) </span><input type="radio" class="radioTestB" name="respuesta2" data-whatever="Mas o menos" value=1/><a id=respuesta2p2>Mas o menos</a></p> 
        <p><span>C)</span><input type="radio" class="radioTest" name="respuesta2" data-whatever="No" value=0/><a id=respuesta3p2>No</a></p>
        <br>
        <h3>3) <a id="pregunta3">¿Es facil encontrar lo que buscas en la aplicacion?</a></h3>
        <br>
        <p><span>A)</span><input type="radio" class="radioTest" name="respuesta3" data-whatever="Si" value=2/><a id=respuesta1p3>Si</a></p> 
        <p><span>B) </span><input type="radio" class="radioTestB" name="respuesta3" data-whatever="Mas o menos" value=1/><a id=respuesta2p3>Mas o menos</a></p> 
        <p><span>C)</span><input type="radio" class="radioTest" name="respuesta3" data-whatever="No" value=0/><a id=respuesta3p3>No</a></p>
        <br>
        <h3>4) <a id="pregunta4">¿La aplicacion cumple tus expectativas?</a></h3>
        <br>
        <p><span>A)</span><input type="radio" class="radioTest" name="respuesta4" data-whatever="Si" value=2/><a id=respuesta1p4>Si</a></p> 
        <p><span>B) </span><input type="radio" class="radioTestB" name="respuesta4" data-whatever="Mas o menos" value=1/><a id=respuesta2p4>Mas o menos</a></p> 
        <p><span>C)</span><input type="radio" class="radioTest" name="respuesta4" data-whatever="No" value=0/><a id=respuesta3p4>No</a></p>
        <br>
        <h3>5) <a id="pregunta5">¿Como fue la intereaccion con nuestros empleados?</a></h3>
        <br>
        <p><span>A)</span><input type="radio" class="radioTest"  name="respuesta5" data-whatever="Muy buena" value=2/><a id=respuesta1p5>Muy buena</a></p> 
        <p><span>B) </span><input type="radio" class="radioTestB" name="respuesta5" data-whatever="Normal" value=1/><a id=respuesta2p5>Normal</a></p> 
        <p><span>C)</span><input type="radio" class="radioTest" name="respuesta5" data-whatever="Mala" value=0/><a id=respuesta3p5>Mala</a></p>
        <br>
        <h3>6) <a id="pregunta6">¿Hasta ahora tu experiencia como la definirias?</a></h3>
        <br>
        <p><span>A)</span><input type="radio" class="radioTest" name="respuesta6" data-whatever="Muy buena" value=2/><a id=respuesta1p6>Muy buena</a></p> 
        <p><span>B) </span><input type="radio" class="radioTestB" name="respuesta6" data-whatever="Normal" value=1/><a id=respuesta2p6>Normal</a></p> 
        <p><span>C)</span><input type="radio" class="radioTest" name="respuesta6" data-whatever="Mala" value=0/><a id=respuesta3p6>Mala</a></p>

        <button id="FinalizarEncuesta" type="button" class="btn btn-success btn-block">Finalizar Encuesta</button>
    
      </div>
      <!-- Fin: Encuesta Satisfaccion -->



      <!--Inicio: Ventana Modal Advertencia Perfil Inversor-->
      <div class="modal fade" id="MensajeAdvertenciaPerfilInversor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">New message</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form>
                <div class="form-group">
                  <label for="loging-dni" class="col-form-label">No se permite acceder a las ofertas de inversion hasta que se realize el test de perfil de inversion.</label>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-success" id="realizarTestDeInversion">Realizar Test de Perfil de Inversion</button>
            </div>
          </div>
        </div>
      </div>
      </div>
      <!--Fin: Ventana Modal Advertencia Perfil Inversor-->


      <!-- Inicio: Pie de Pagina -->
      <footer id="pieDePagina">
        <div class="bp-wraper"> 
          <div class="bp-col-2">
            <p>InvertirOnline 0810-1582-IOL&#40;465&#41;</p>
            <p>Juan María Gutiérrez 1150, B1613 Los Polvorines, Buenos Aires</p>
          </div>
          <div class="bp-col-2 bp-right">
            <p>
              <span>
                <a href="https://www.facebook.com" title="Facebook" target="_blank">
                  <img src="https://s3.amazonaws.com/resources.invertironline.com/external/productos/images/facebook.png" alt=""/>
                </a>
              </span>
              <span>
                <a href="https://twitter.com" title="Twitter" target="_blank">
                  <img src="https://s3.amazonaws.com/resources.invertironline.com/external/productos/images/twitter.png" alt=""/>
                </a>
              </span>     
            </p>
            <p>Designed by <a title="sample text" target="_blank">Sample Text</a></p>
          </div>
        </div>
      </footer>
      <!-- Fin: Pie de Pagina -->


<!-- Aqui se carga la libreria jquery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js">
  </script>
  <!-- Aqui indicamos donde esta nuetro código java Script -->
  <script type="text/javascript" src="../js/encuestaSatisfaccion.js"></script>
  <link rel="StyleSheet" href="../css/estiloTodoInversiones.css" type="text/css">

  <script type="text/javascript" src="../bootstrap/jsBootstrap/bootstrap.min.js"></script>
  <link rel="StyleSheet" href="../bootstrap/cssBootstrap/bootstrap.css" type="text/css">
  <link rel="StyleSheet" href="../bootstrap/cssBootstrap/bootstrap.min.css" type="text/css">

</body>
</html>      