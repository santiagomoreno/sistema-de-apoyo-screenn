<?php 
require_once 'conexion.php';
require_once 'Cliente.php';
require_once 'PerfilDeInversor.php';

function actualizarPerfilInversor(){
  $mysqli = getConn();
  $Respuesta1 = $_POST['respuesta1'];
  $Respuesta2 = $_POST['respuesta2'];
  $Respuesta3 = $_POST['respuesta3'];
  $Respuesta4 = $_POST['respuesta4'];
  $Respuesta5 = $_POST['respuesta5'];
  $Respuesta6 = $_POST['respuesta6'];
  $Respuesta7 = $_POST['respuesta7'];
  $Respuesta8 = $_POST['respuesta8'];
  $Respuesta9 = $_POST['respuesta9'];
  $Respuesta10 = $_POST['respuesta10'];
  /*inicia proceso de registro*/
  $query = "start transaction;";
  $result1 = $mysqli->query($query);
  
  if($result1){

  		$resultado = $Respuesta1 + $Respuesta2 + $Respuesta3 + $Respuesta4 + $Respuesta5 + $Respuesta6 + $Respuesta7 + $Respuesta8 + $Respuesta9 + $Respuesta10;
  		if($resultado<19){
  			session_start();
  			$_SESSION['cliente']->setPerfilDeInversor(PerfilDeInversor::Conservador);
  			$queryActualizar = $_SESSION['cliente']->actualizar();
  			$result = $mysqli->query($queryActualizar);
  			if($result){
	  			/*si se crea correctamente el cliente, confirmo los datos anteriormente ingresados*/
	  			$query = "commit;";
	  			$mysqli->query($query);
	  			return true;
	 		}else{
	  			/*si falla al crear el cliente*/
	  			$query = "rollback;";
	  			$mysqli->query($query);
	  			$_SESSION['cliente']->setPerfilDeInversor(0);
	  			return false;
	  		}
  		}elseif($resultado>=19&&$resultado<27){
  			session_start();
  			$_SESSION['cliente']->setPerfilDeInversor(PerfilDeInversor::Moderado);
  			$queryActualizar = $_SESSION['cliente']->actualizar();
  			$result = $mysqli->query($queryActualizar);
  			if($result){
	  			/*si se crea correctamente el cliente, confirmo los datos anteriormente ingresados*/
	  			$query = "commit;";
	  			$mysqli->query($query);
	  			return true;
	  		}else{
	  			/*si falla al crear el cliente*/
	  			$query = "rollback;";
	  			$mysqli->query($query);
	  			$_SESSION['cliente']->setPerfilDeInversor(0);
	  			return false;
	  		}
  		}elseif($resultado>=27){
  			session_start();
  			$_SESSION['cliente']->setPerfilDeInversor(PerfilDeInversor::Arriesgado);
  			$queryActualizar = $_SESSION['cliente']->actualizar();
  			$result = $mysqli->query($queryActualizar);
  			if($result){
	  			/*si se crea correctamente el cliente, confirmo los datos anteriormente ingresados*/
	  			$query = "commit;";
	  			$mysqli->query($query);
	  			return true;
	  		}else{
	  			/*si falla al crear el cliente*/
	  			$query = "rollback;";
	  			$mysqli->query($query);
	  			$_SESSION['cliente']->setPerfilDeInversor(0);
	  			return false;
	  		}
  		} 
  }else{
	  	/*si falla la primera instruccion*/
	  	$query = "rollback;";
	  	$mysqli->query($query);
	  	return false;
  }

  /*finaliza proceso de registro*/

  return true;
}

echo actualizarPerfilInversor();

?>