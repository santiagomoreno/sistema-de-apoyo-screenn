<?php 
require_once 'conexion.php';
require_once 'Cliente.php';
require_once 'EstadoSatisfaccion.php';

function realizarEncuestaSatisfaccion(){
  $mysqli = getConn();
  $ValorRespuesta1 = $_POST['valorRespuesta1'];
  $ValorRespuesta2 = $_POST['valorRespuesta2'];
  $ValorRespuesta3 = $_POST['valorRespuesta3'];
  $ValorRespuesta4 = $_POST['valorRespuesta4'];
  $ValorRespuesta5 = $_POST['valorRespuesta5'];
  $ValorRespuesta6 = $_POST['valorRespuesta6'];
  /*inicia proceso de registro*/
  $query = "start transaction;";
  $result1 = $mysqli->query($query);
  
  if($result1){

  		$resultado = $ValorRespuesta1 + $ValorRespuesta2 + $ValorRespuesta3 + $ValorRespuesta4 + $ValorRespuesta5 + $ValorRespuesta6;

  		if($resultado<6){
    			session_start();
          $queryCrearEncuesta = "INSERT INTO encuestasatisfaccion (cliente,estadodesatisfaccion) VALUES (".$_SESSION['cliente']->getId().",".EstadoSatisfaccion::Desconforme.");";
          $resultCrearEncuesta = $mysqli->query($queryCrearEncuesta);
    			if($queryCrearEncuesta){
    	  			/*si se crea correctamente el cliente, confirmo los datos anteriormente ingresados*/
    	  			$query = "commit;";
    	  			$mysqli->query($query);
    	  			return true;
  	 		  }else{
    	  			/*si falla al crear el cliente*/
    	  			$query = "rollback;";
    	  			$mysqli->query($query);
    	  			return false;
  	  		}
  		}elseif($resultado>=6&&$resultado<10){
    			session_start();
          $queryCrearEncuesta = "INSERT INTO encuestasatisfaccion (cliente,estadodesatisfaccion) VALUES (".$_SESSION['cliente']->getId().",".EstadoSatisfaccion::Conforme.");";
          $resultCrearEncuesta = $mysqli->query($queryCrearEncuesta);
    			if($resultCrearEncuesta){
    	  			/*si se crea correctamente el cliente, confirmo los datos anteriormente ingresados*/
    	  			$query = "commit;";
    	  			$mysqli->query($query);
    	  			return true;
  	  		}else{
    	  			/*si falla al crear el cliente*/
    	  			$query = "rollback;";
    	  			$mysqli->query($query);
    	  			return false;
  	  		}
  		}elseif($resultado>=10){
    			session_start();
          $queryCrearEncuesta = "INSERT INTO encuestasatisfaccion (cliente,estadodesatisfaccion) VALUES (".$_SESSION['cliente']->getId().",".EstadoSatisfaccion::MuySatisfecho.");";
          $resultCrearEncuesta = $mysqli->query($queryCrearEncuesta);
    			if($resultCrearEncuesta){
    	  			/*si se crea correctamente el cliente, confirmo los datos anteriormente ingresados*/
    	  			$query = "commit;";
    	  			$mysqli->query($query);
    	  			return true;
  	  		}else{
    	  			/*si falla al crear el cliente*/
    	  			$query = "rollback;";
    	  			$mysqli->query($query);
    	  			return false;
  	  		}
  		} 
  }else{
	  	/*si falla la primera instruccion*/
	  	$query = "rollback;";
	  	$mysqli->query($query);
	  	return false;
  }

  /*finaliza proceso de registro*/

  return false;
}

echo realizarEncuestaSatisfaccion();

?>