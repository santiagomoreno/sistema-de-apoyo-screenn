<?php 
require_once 'conexion.php';

function registrarCliente(){
  $mysqli = getConn();
  $Nombre = $_POST['nombre'];
  $Apellido = $_POST['apellido'];
  $Domicilio = $_POST['domicilio'];
  $DNI = $_POST['dni'];
  $FechaNac = $_POST['fechaNac'];
  $Banco = $_POST['banco'];
  $NroCuenta = $_POST['nroCuenta'];
  $Email = $_POST['email'];
  $Password = $_POST['password'];
  $FechaActual = date('Y-m-d');
  /*inicia proceso de registro*/
  $query = "start transaction;";
  $result1 = $mysqli->query($query);
  
  if($result1){

	  	/*si se ejecuta correctamente la primera instruccion, creo una cuenta comitente*/
	  	$query = "INSERT INTO cuentacomitente (nroCuenta, saldo) VALUES ('$NroCuenta','0');";
	  	$result2 = $mysqli->query($query);

	  	if($result2){
	  		/*si se crea correctamente la cuenta comitente, creo un cliente*/
	  		$query = "INSERT INTO cliente (nombre,apellido,domicilio,dni,fechaNacimiento,email,password,fechaIngreso,cuentaComitente) VALUES ('$Nombre','$Apellido','$Domicilio','$DNI','$FechaNac','$Email','$Password','$FechaActual','$NroCuenta');";
	  		$result3 = $mysqli->query($query);

	  		if($result3){
	  			/*si se crea correctamente el cliente, confirmo los datos anteriormente ingresados*/
	  			$query = "commit;";
	  			$mysqli->query($query);
	  			return true;
	  		}else{
	  			/*si falla al crear el cliente*/
	  			$query = "rollback;";
	  			$mysqli->query($query);
	  			return false;
	  		}

	  	}else{
	  		/*si falla al crear la cuenta comitente*/
			$query = "rollback;";
	  		$mysqli->query($query);
	  		return false;
	  	}

  }else{
	  	/*si falla la primera instruccion*/
	  	$query = "rollback;";
	  	$mysqli->query($query);
	  	return false;
  }

  /*finaliza proceso de registro*/

  return false;
}

echo registrarCliente();

?>