<?php 
require_once 'conexion.php';

function getBancos(){
  $mysqli = getConn();
  $query = "SELECT * FROM banco";
  $result = $mysqli->query($query);
  $bancos = '<option value="0">Seleccione un Banco</option>';
  while($row = $result->fetch_array(MYSQLI_ASSOC)){
    $bancos .= "<option value='$row[idBanco]'>$row[nombreBanco]</option>";
  }
  return $bancos;
}

echo getBancos();

?>