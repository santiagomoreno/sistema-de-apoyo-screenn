$(document).ready(function(){
  // Todo lo que esté aca a dentro se ejcuta instantaneamente
    // al terminar de cargarse la página.
  $('#RegistroModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-title').text(recipient)
  $.ajax({
      type: 'POST',
      url: 'php/cargarBancos.php'
    })
    .done(function(listas_rep){
      $('#register-banco').html(listas_rep)
    })
    .fail(function(){
      alert('Hubo un error al cargar los Bancos')
    })
})

  $('#IngresoModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-title').text(recipient)
})

$('#register-confirmRegister').on('click', function(){
  $("#alertaNombre").hide();
  $("#alertaApellido").hide();
  $("#alertaDomicilio").hide();
  $("#alertaSinDNI").hide();
  $("#alertaSinFechaNac").hide();
  $("#alertaBanco").hide();
  $("#alertaSinNroDeCuenta").hide();
  $("#alertaSinEmail").hide();
  $("#alertaSinPassword").hide();
  $("#alertaDisclaimer").hide();
  ///////////////////////////////////////////////////////
  //oculto alertas por si quedo alguno activado antes////
  ///////////////////////////////////////////////////////
  $("#alertaDniInvalido").hide();
  $("#alertaPasswordInvalida").hide();
  $("#alertaEdadInvalida").hide();
  $("#alertaNroDeCuentaInvalido").hide();
  $("#alertaEmailInvalido").hide();
  ///////////////////////////////////////////////////////
  //fin ocultar alerta///////////////////////////////////
  ///////////////////////////////////////////////////////
  var nombre = $('#register-name').val()
  var apellido = $('#register-surname').val()
  var domicilio = $('#register-domicilio').val()
  var dni = $('#register-dni').val()
  var fechaNac = $('#register-fechaNacimiento').val()
  var banco = $('#register-banco').val()
  var nroCuenta = $('#register-numberAccount').val()
  var email = $('#register-email').val()
  var password = $('#register-password').val()
  var terminosDeUso = $('#register-termUse').is(':checked');
  if((typeof nombre === 'undefined') || (nombre === '') 
    || (typeof apellido === 'undefined') || (apellido === '')
    || (typeof domicilio === 'undefined') || (domicilio === '') 
    || (typeof dni === 'undefined') || (dni === '')
    || (typeof fechaNac === 'undefined') || (fechaNac === '')
    || (typeof banco === 'undefined') || (banco === '') || (banco === '0')
    || (typeof nroCuenta === 'undefined') || (nroCuenta === '')
    || (typeof email === 'undefined') || (email === '')
    || (typeof password === 'undefined')|| (password === '')
    || (!terminosDeUso)){
    alert('Debe completar todos los campos para poder registrarse')
    if((typeof nombre === 'undefined') || (nombre === '')){
      $("#alertaNombre").show();
    }
    if((typeof apellido === 'undefined') || (apellido === '')){
      $("#alertaApellido").show();
    }
    if((typeof domicilio === 'undefined') || (domicilio === '')){
      $("#alertaDomicilio").show();
    }
    if((typeof dni === 'undefined') || (dni === '')){
      $("#alertaSinDNI").show();
    } 
    if((typeof fechaNac === 'undefined') || (fechaNac === '')){
      $("#alertaSinFechaNac").show();
    } 
    if((typeof banco === 'undefined') || (banco === '') || (banco === '0')){
      $("#alertaBanco").show();
    } 
    if((typeof nroCuenta === 'undefined') || (nroCuenta === '')){
      $("#alertaSinNroDeCuenta").show();
    } 
    if((typeof email === 'undefined') || (email === '')){
      $("#alertaSinEmail").show();
    } 
    if((typeof password === 'undefined')|| (password === '')){
      $("#alertaSinPassword").show();
    } 
    if((!terminosDeUso)){
      $("#alertaDisclaimer").show();
    } 
  }else{
      //////////////////////////////////////
      //variables de validacion de datos////
      //////////////////////////////////////
      var dniValido = false; //dni correcto
      var fechaNacValida = false; //fecha de nacimiento correcta
      var nroCuentaValido = false; //nro cuenta correcto
      var emailValido = false; //email correcto
      var passwordValida = false; //contraseña correcta
      /////////////////////////////////////////////
      //fin de variables de validacion de datos////
      /////////////////////////////////////////////

      /////////////////////////////////////
      //comprobar que el dni sea valido////
      /////////////////////////////////////
      var ochoDigitos = /[0-9]{8}/;
      //evalua contraseña
      if(ochoDigitos.test(dni.trim())&&dni<9){
          dniValido = true; //confirma dni valido
      }else{
          $("#alertaDniInvalido").show();  //alerta dni invalido
      }
      /////////////////////////////////
      //fin comprobacion dni valido////
      /////////////////////////////////

      ////////////////////////////////////////////
      //comprobar que la contraseña sea valida////
      ////////////////////////////////////////////
      var letrasMinus = /[a-z]{1,}/;
      var letrasMayus = /[A-Z]{1,}/;
      var numeros = /[0-9]{1,}/;
      //evalua contraseña
      if(letrasMinus.test(password.trim())&&letrasMayus.test(password.trim())&&numeros.test(password.trim())&&password.length >= 8){
          passwordValida = true; //confirma contraseña valida
      }else{
          $("#alertaPasswordInvalida").show();  //alerta contraseña invalida
      }
      ////////////////////////////////////////
      //fin comprobacion contraseña valida////
      ////////////////////////////////////////

      ///////////////////////////////////////
      //comprobar que la fecha sea valida////
      ///////////////////////////////////////
      var arrayFecha = fechaNac.split("-") //array de la fecha 0:año 1:mes 2:dia
      var dateFechaNac = new Date() //date de fecha de nacimiento
      dateFechaNac.setFullYear(arrayFecha[0]) //setear año de la fecha de nacimiento
      dateFechaNac.setMonth(arrayFecha[1]-1)  //setear mes de la fecha de nacimiento
      dateFechaNac.setDate(arrayFecha[2])     //setear dia de la fecha de nacimiento

      var fechaHoy = new Date() //fecha actual
      fechaHoy.setFullYear(fechaHoy.getFullYear()-18) //fecha actual menos 18 años
      //evalua edad
      if(fechaHoy<dateFechaNac){
        $("#alertaEdadInvalida").show(); //alerta edad invalida
      }else{
        fechaNacValida = true; //confirma fecha nacimiento valida
      }
      ////////////////////////////////////////
      //fin comprobacion fecha valida/////////
      ////////////////////////////////////////

      ///////////////////////////////////////////////
      //comprobar que el nro de cuenta sea valido////
      ///////////////////////////////////////////////
      //evalua nro de cuenta
      if(nroCuenta.length >= 20){
        nroCuentaValido = true; //confirma nro de cuenta valido
      }else{
        $("#alertaNroDeCuentaInvalido").show(); //alerta nro de cuenta invalido
      }
      //////////////////////////////////////////////
      //fin comprobacion nro de cuenta valido///////
      //////////////////////////////////////////////

      ///////////////////////////////////////
      //comprobar que el email sea valido////
      ///////////////////////////////////////
      var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
      //evalua email
      if(regex.test(email.trim())){
        emailValido = true; //confirma email valido
      }else{
        $("#alertaEmailInvalido").show(); //alerta email invalido
      }
      ////////////////////////////////////////
      //fin comprobacion email valido/////////
      ////////////////////////////////////////

      if(dniValido&&fechaNacValida&&nroCuentaValido&&emailValido&&passwordValida){
          $.ajax({
              type: 'POST',
              url: 'php/registrarCliente.php',
              data: {'nombre': nombre,
                      'apellido': apellido,
                      'domicilio': domicilio,
                      'dni': dni,
                      'fechaNac': fechaNac,
                      'banco': banco,
                      'nroCuenta': nroCuenta,
                      'email': email,
                      'password': password}
          })
          .done(function(listas_rep){
              if(listas_rep==true){
                alert('el cliente se registro correctamente')
                $('#RegistroModal').modal('toggle');
              }else{
                alert('no se pudo registrar al cliente')
              }
          })
          .fail(function(){
              alert('Hubo un error al registrar el cliente')
          })
      } 
  } 
})

$('#loging-confirmIngreso').on('click', function(){
    var dni = $('#loging-dni').val()
    var password = $('#loging-password').val()
    $.ajax({
      type: 'POST',
      url: 'php/ingresar.php',
      data: {'dni': dni,
              'password': password},
      cache:"false",
      success:function(data){
        if(data=="1"){
          location.href ="paginas/user.php";
        }else{
          alert('el usuario o la contraseña es incorrecto/a');
        }
      }        

    })    
  })

  $('#register-dni').keydown(function (e) {
        $("#alertaSinDNI").hide();
        $("#alertaDniCaracterInvalido").hide()
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
            (e.keyCode == 65 && e.ctrlKey === true) || 
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 return;
        }
 
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
            $("#alertaDniCaracterInvalido").show()
        }
  });

  $('#register-numberAccount').keypress(function (e) {
        $("#alertaSinNroDeCuenta").hide();
        $("#alertaNroDeCuentaCaracterInvalido").hide()
        if($.inArray(e.keyCode, [20,127, 8, 9, 27, 13]) !== -1 ||
            (e.keyCode == 65 && e.ctrlKey === true) || 
            (e.keyCode >= 35 && e.keyCode <= 39)){
            return;
        }
        if((e.shiftKey || (e.keyCode < 65 || e.keyCode > 90)) && (e.keyCode < 48 || e.keyCode > 57)){
          e.preventDefault();
          $("#alertaNroDeCuentaCaracterInvalido").show()
        }
  });

})