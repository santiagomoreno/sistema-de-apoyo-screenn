<?php
require_once '../php/Cliente.php';
session_start();

if((!isset($_SESSION['cliente']))){
  header("location:../index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Todo Inversiones</title>
  <link rel="shortcut icon" href="../imagenes/manos.png">
</head>
<body>
      <!-- Inicio: Encabezado -->      
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href=""><img src="../imagenes/iconoTodoInversioness2.png" alt=""/> TodoInversiones.com</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" id="perfil"><?php echo $_SESSION['cliente']->getNombre(); ?></a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" id="invertir">Ofertas de Invercion</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" id="portafolio">Portafolio</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" id="encuesta">Encuesta Satisfaccion</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="cerrarSesion" href="../php/cerrarSession.php">Cerrar Sesion</a>
            </li>
          </ul>          
        </div>
      </nav>
      <!-- Fin: Encabezado -->


      <!-- Inicio: Test Inversor -->
      <div id="TestInversor">
        <h1>Test Perfil Inversor</h1>
        <h3>1) <a id="pregunta1">¿Preferis la seguridad o el riesgo?</a></h3>
        <br>
        <p><span>A)</span><input type="radio" class="radioTest" name="respuesta1" value="1"/><a id=respuesta1p1>Prefiero la seguriadad</a></p> 
        <p><span>B) </span><input type="radio" class="radioTestB" name="respuesta1" value="2"/><a id=respuesta2p1>Depende</a></p> 
        <p><span>C)</span><input type="radio" class="radioTest" name="respuesta1" value="3"/><a id=respuesta3p1>Prefiero el riesgo</a></p> 
        <br>
        <h3>2) <a id="pregunta2">¿Te interesa mas ganar mucho dinero o asegurar el dinero que ya tenes?</a></h3>
        <br>
        <p><span>A)</span><input type="radio" class="radioTest" name="respuesta2" value=1/><a id=respuesta1p2>Asegurar el que ya tengo</a></p> 
        <p><span>B) </span><input type="radio" class="radioTestB" name="respuesta2" value=2/><a id=respuesta2p2>Un poco de ambos</a></p> 
        <p><span>C)</span><input type="radio" class="radioTest" name="respuesta2" value=3/><a id=respuesta3p2>Ganar mucho dinero</a></p>
        <br>
        <h3>3) <a id="pregunta3">¿Pensas vivir de las inversiones a futuro?</a></h3>
        <br>
        <p><span>A)</span><input type="radio" class="radioTest" name="respuesta3" value=1/><a id=respuesta1p3>No</a></p> 
        <p><span>B) </span><input type="radio" class="radioTestB" name="respuesta3" value=2/><a id=respuesta2p3>Nose</a></p> 
        <p><span>C)</span><input type="radio" class="radioTest" name="respuesta3" value=3/><a id=respuesta3p3>Si</a></p>
        <br>
        <h3>4) <a id="pregunta4">¿Crees que las inversiones es una manera de que el dinero no se devalue?</a></h3>
        <br>
        <p><span>A)</span><input type="radio" class="radioTest" name="respuesta4" value=1/><a id=respuesta1p4>Si</a></p> 
        <p><span>B) </span><input type="radio" class="radioTestB" name="respuesta4" value=2/><a id=respuesta2p4>Mas o menos</a></p> 
        <p><span>C)</span><input type="radio" class="radioTest" name="respuesta4" value=3/><a id=respuesta3p4>No</a></p>
        <br>
        <h3>5) <a id="pregunta5">¿Crees que las inversiones es una manera de ganar mucho dinero?</a></h3>
        <br>
        <p><span>A)</span><input type="radio" class="radioTest"  name="respuesta5" value=1/><a id=respuesta1p5>No</a></p> 
        <p><span>B) </span><input type="radio" class="radioTestB" name="respuesta5" value=2/><a id=respuesta2p5>Mas o menos</a></p> 
        <p><span>C)</span><input type="radio" class="radioTest" name="respuesta5" value=3/><a id=respuesta3p5>Si</a></p>
        <br>
        <h3>6) <a id="pregunta6">¿Preferis arriesgar para ganar mucho o no ganar mucho pero seguro?</a></h3>
        <br>
        <p><span>A)</span><input type="radio" class="radioTest" name="respuesta6" value=1/><a id=respuesta1p6>Asegurar</a></p> 
        <p><span>B) </span><input type="radio" class="radioTestB" name="respuesta6" value=2/><a id=respuesta2p6>Depende el riesgo y lo que se gane con el</a></p> 
        <p><span>C)</span><input type="radio" class="radioTest" name="respuesta6" value=3/><a id=respuesta3p6>Arriesgar</a></p>
        <br>
        <h3>7) <a id="pregunta7">¿Si llegases a perder te afectaria mucho en tu economia?</a></h3>
        <br>
        <p><span>A)</span><input type="radio" class="radioTest" name="respuesta7" value=1/><a id=respuesta1p7>No</a></p> 
        <p><span>B) </span><input type="radio" class="radioTestB" name="respuesta7" value=2/><a id=respuesta2p7>Mas o menos</a></p> 
        <p><span>C)</span><input type="radio" class="radioTest" name="respuesta7" value=3/><a id=respuesta3p7>Si</a></p>
        <br>
        <h3>8) <a id="pregunta8">Cuando inviertes, ¿compras diferentes activos en diferentes regiones o apuestas todo a un mismo tipo de activo?</a></h3>
        <br>
        <p><span>A)</span><input type="radio" class="radioTest" name="respuesta8" value=1/><a id=respuesta1p8>Compro diversos activos en diferentes regiones</a></p> 
        <p><span>B) </span><input type="radio" class="radioTestB" name="respuesta8" value=2/><a id=respuesta2p8>Generalmente diversifico mis inversiones, pero aveces me gusta apostar todo a algo que esta llendo bien</a></p> 
        <p><span>C)</span><input type="radio" class="radioTest" name="respuesta8" value=3/><a id=respuesta3p8>Arriesgo todo a un mismo tipo de activo</a></p>
        <br>
        <h3>9) <a id="pregunta9">Si ganaras la loteria y desearas invertir, ¿invertirias todo tu dinero o conservarias una parte para momentos dificiles?</a></h3>
        <br>
        <p><span>A)</span><input type="radio" class="radioTest" name="respuesta9" value=1/><a id=respuesta1p9>Conservaria una parte por seguridad</a></p> 
        <p><span>B) </span><input type="radio" class="radioTestB" name="respuesta9" value=2/><a id=respuesta2p9>Nose</a></p> 
        <p><span>C)</span><input type="radio" class="radioTest" name="respuesta9" value=3/><a id=respuesta3p9>Invertiria todo para ganar mas</a></p>
        <br>
        <h3>10) <a id="pregunta10">Imagina qué sensación tendrías si tus inversiones pierden un 60%, ¿Te sentirías cómodo o te quitaría el sueño por la noche?</a></h3>
        <br>
        <p><span>A)</span><input type="radio" class="radioTest" name="respuesta10" value=1/><a id=respuesta1p10>No podria dormir</a></p> 
        <p><span>B) </span><input type="radio" class="radioTestB" name="respuesta10" value=2/><a id=respuesta2p10>Estaria algo intranquilo, pero hay que mantener la calma</a></p> 
        <p><span>C)</span><input type="radio" class="radioTest" name="respuesta10" value=3/><a id=respuesta3p10>No me importa ya mejorara, mejor compro mas ahora que esta mas barato</a></p>

        <button id="FinalizarTest" type="button" class="btn btn-success btn-block">Finalizar Test</button>
    
      </div>
      <!-- Fin: Test Inversor -->


      <!--Inicio: Ventana Modal Advertencia Perfil Inversor-->
      <div class="modal fade" id="MensajeAdvertenciaPerfilInversor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">New message</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form>
                <div class="form-group">
                  <label for="loging-dni" class="col-form-label">No se permite acceder a las ofertas de inversion hasta que se realize el test de perfil de inversion.</label>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-success" id="realizarTestDeInversion">Realizar Test de Perfil de Inversion</button>
            </div>
          </div>
        </div>
      </div>
      </div>
      <!--Fin: Ventana Modal Advertencia Perfil Inversor-->



      <!-- Inicio: Pie de Pagina -->
      <footer id="pieDePagina">
        <div class="bp-wraper"> 
          <div class="bp-col-2">
            <p>InvertirOnline 0810-1582-IOL&#40;465&#41;</p>
            <p>Juan María Gutiérrez 1150, B1613 Los Polvorines, Buenos Aires</p>
          </div>
          <div class="bp-col-2 bp-right">
            <p>
              <span>
                <a href="https://www.facebook.com" title="Facebook" target="_blank">
                  <img src="https://s3.amazonaws.com/resources.invertironline.com/external/productos/images/facebook.png" alt=""/>
                </a>
              </span>
              <span>
                <a href="https://twitter.com" title="Twitter" target="_blank">
                  <img src="https://s3.amazonaws.com/resources.invertironline.com/external/productos/images/twitter.png" alt=""/>
                </a>
              </span>     
            </p>
            <p>Designed by <a title="sample text" target="_blank">Sample Text</a></p>
          </div>
        </div>
      </footer>
      <!-- Fin: Pie de Pagina -->












  <!-- Aqui se carga la libreria jquery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js">
  </script>
  <!-- Aqui indicamos donde esta nuetro código java Script -->
  <script type="text/javascript" src="../js/testPerfilDeInversor.js"></script>
  <link rel="StyleSheet" href="../css/estiloTodoInversiones.css" type="text/css">

  <script type="text/javascript" src="../bootstrap/jsBootstrap/bootstrap.min.js"></script>
  <link rel="StyleSheet" href="../bootstrap/cssBootstrap/bootstrap.css" type="text/css">
  <link rel="StyleSheet" href="../bootstrap/cssBootstrap/bootstrap.min.css" type="text/css">
</body>
</html>