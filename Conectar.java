package Clases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Conectar {

	public Statement sentencia;
	public ResultSet resultado;
	private static Connection conn;
	private static final String driver="com.mysql.jdbc.Driver";
	private static final String user ="root";
	private static final String password = "123456";
	private static final String url ="jdbc:mysql://localhost:3306/todoinversiones";
	
	
	public Conectar() throws ClassNotFoundException{
		conn=null;
		try{
			Class.forName(driver);
			conn=DriverManager.getConnection(url, user, password);
			sentencia= conn.createStatement();
			if(conn!=null) {
				System.out.println("Establecida la conexion con la base de datos");
			}
		}
		catch (Exception e){
			System.out.println("error"+ e);
		}
	}
	public Connection getConnection() {
		return conn;
	}
	public void desconectar() {
		conn= null;
		if(conn==null)System.out.println("desconectado de la base de datos");

	}

	public static void main(String[] args) throws ClassNotFoundException {
			Conectar nuevo=new Conectar();
			nuevo.desconectar();
	}

}
