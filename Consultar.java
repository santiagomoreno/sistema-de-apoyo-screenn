package Clases;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Consultar {
	private Conectar conexion;

	public Consultar() throws ClassNotFoundException {
		conexion=new Conectar();

	}
	public void consultar_cantOperaciones(String desde, String hasta ) throws ClassNotFoundException, SQLException {
		try {
		conexion.resultado=conexion.sentencia.executeQuery("select * from transaccionactivo where fecha>="+desde+" and fecha=<"+hasta+";");
		ResultSet rs=conexion.resultado;
		int cant=0;
		if(rs!=null) {
			System.out.println("cantidad de operaciones realizadas");
			System.out.println("desde: "+ desde+" hasta: "+ hasta);

			while(rs.next()) {
				cant++;	
			}
			System.out.println(cant);

		}		
		}catch(Exception e) {
			System.out.println("fallo la consulta "+ e);
		}
	}
	public void consultar_clientesRegistrados() throws ClassNotFoundException, SQLException {
		try {
		conexion.resultado=conexion.sentencia.executeQuery("select * from cliente;");
		int cont=0;
		ResultSet rs=conexion.resultado;
		if(rs!=null) {
			System.out.print("cantidad de usuarios registrados: ");
			
			while(rs.next()) {
				cont++;
			}
			System.out.println(cont);
		}else System.out.println("no hay clientes registrados");

		}catch(Exception e) {
			System.out.println("fallo la consulta "+ e);
		}
	}
	public void consultar_comisionesAbonadas() throws ClassNotFoundException, SQLException {
		try {
		conexion.resultado=conexion.sentencia.executeQuery("select * from transaccionactivo;");
		ResultSet rs=conexion.resultado;
		int cont=0;
		int valorTransaccion=100;
		if(rs!=null) {
			System.out.print("El total de las comisiones abonadas hasta el momento son de: ");
			
			while(rs.next()) {
				cont++;
			}
			System.out.println("$"+cont*valorTransaccion);
		}		
		}catch(Exception e) {
			System.out.println("fallo la consulta "+ e);
		}
	}
	public void desconectar(){
		conexion.desconectar();
	}
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Consultar nuevo=new Consultar();
		nuevo.consultar_clientesRegistrados();
		nuevo.consultar_comisionesAbonadas();
		nuevo.desconectar();
	}

}
