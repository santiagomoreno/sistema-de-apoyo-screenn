<?php 
require_once 'conexion.php';

function crearCategoria(){
  $mysqli = getConn();
  $NombreCategoria = $_POST['nombreCategoria'];
  /*inicia proceso de registro*/
  $query = "start transaction;";
  $result1 = $mysqli->query($query);
  
  if($result1){

	  	/*si se ejecuta correctamente la primera instruccion, creo una cuenta comitente*/
	  	$query = "INSERT INTO categoria (nombrecategoria) VALUES ('$NombreCategoria');";
	  	$result2 = $mysqli->query($query);

	  	if($result2){
	  		/*si se crea correctamente el cliente, confirmo los datos anteriormente ingresados*/
	  		$query = "commit;";
	  		$mysqli->query($query);
	  		return true;

	  	}else{
	  		/*si falla al crear la cuenta comitente*/
			$query = "rollback;";
	  		$mysqli->query($query);
	  		return false;
	  	}
	  	
  }else{
	  	/*si falla la primera instruccion*/
	  	$query = "rollback;";
	  	$mysqli->query($query);
	  	return false;
  }

  /*finaliza proceso de registro*/

  return false;
}

echo crearCategoria();

?>