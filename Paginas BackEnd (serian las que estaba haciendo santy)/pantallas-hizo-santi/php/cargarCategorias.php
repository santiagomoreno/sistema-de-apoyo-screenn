<?php 
require_once 'conexion.php';

// Funcion de coenxion.php
function getCategorias(){
  $mysqli = getConn();
  $query = 'SELECT * FROM categoria';
  $result = $mysqli->query($query);
  $categoria = ' <option value="0" disabled selected>Elegir categoria</option>';
  // La consulta me crea un array asociado a la BD e iterará
  // mientras encuentre registros.
  while($row = $result->fetch_array(MYSQLI_ASSOC)){
    $categoria .= "<option value='$row[idcategoria]'>$row[nombrecategoria]</option>";
  }
  return $categoria;
}

echo getCategorias();
?>