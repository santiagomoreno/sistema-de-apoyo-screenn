$(document).ready(function(){
  // Todo lo que esté aca a dentro se ejcuta instantaneamente
    // al terminar de cargarse la página.

  $('#botonCrear').on('click', function(){
    var nombreCategoria = $('#nombreCategoria').val()
    if((typeof nombreCategoria === 'undefined') || (nombreCategoria === '')){
      alert('Debe escribir un nombre para la categoria')
    }else{
      $.ajax({
        type: 'POST',
        url: 'php/crearCategoria.php',
        data: {'nombreCategoria': nombreCategoria}  
      })   
      .done(function(listas_rep){
        if(listas_rep==true){
          alert('la catagoria se creo correctamente')
        }else{
          alert('no se pudo crear la catagoria')
        }
      })
      .fail(function(){
        alert('Hubo un error al crear la catagoria')
      }) 
    }
  })

})