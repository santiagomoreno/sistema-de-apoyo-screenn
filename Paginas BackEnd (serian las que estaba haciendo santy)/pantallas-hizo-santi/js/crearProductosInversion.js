$(document).ready(function(){
  // Todo lo que esté aca a dentro se ejcuta instantaneamente
    // al terminar de cargarse la página.
  $.ajax({
    type: 'POST',
    url: 'php/cargarCategorias.php'
  })
  .done(function(listas_rep){
    // Si todo sal bien se ejecuta lo siguiente.
    // A todo elemento cuyo id es #carrera se le asigna el html de listas_rep
    $('#categoria').html(listas_rep)
  })
  .fail(function(){
    // Si ocurre algun error se ejecuta lo siguiente.
    alert('Hubo un error al cargar las Categoria')
  })

  
  $('#btnCrear').on('click',function(){
    var nombreProducto = $('#nombre').val()
    var simbolo = $('#simbolo').val()
    var categoria = $('#categoria').val()
    if((typeof nombreProducto === 'undefined') || (nombreProducto === '')
      ||(typeof simbolo === 'undefined') || (simbolo === '')
      ||(categoria == null)){
      alert('Debe completar todos los campos para crear un nuevo producto')
    }else{    
      $.ajax({
            type: 'POST',
            url: 'php/crearProducto.php',
            data: {'nombreProducto': nombreProducto,
              'simbolo': simbolo,
              'categoria': categoria}
          })
          .done(function(listas_rep){
            if(listas_rep==true){
              alert('El producto se creo correctamente')
            }else{             
              alert('No se pudo crear el producto')
            }
          })
          .fail(function(){
            alert('Hubo un error al crear el producto de inversion')
      })
    }
})

})